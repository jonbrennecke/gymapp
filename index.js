// @flow
import { Provider } from 'react-redux';
import { Navigation } from 'react-native-navigation';

import store from './src/redux/store';
import { registerScreens } from './src/screens';
import { SINGLE_SCREEN_NAVIGATOR_PARAMS } from './src/constants';

registerScreens(store, Provider);

Navigation.startSingleScreenApp(SINGLE_SCREEN_NAVIGATOR_PARAMS);
