// @flow
import React, { Component } from 'react';
import { View, StyleSheet, Animated, TouchableOpacity } from 'react-native';
import { Navigator } from 'react-native-navigation';
import { autobind } from 'core-decorators';
import { connect } from 'react-redux';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import map from 'lodash/map';
import first from 'lodash/first';

import { UI_COLORS, TEXT_COLORS, BACKGROUND_LAYER_OPACITY } from '../../constants';
import {
  loadActivities,
  createActivity,
} from '../../redux/activities/actionCreators';
import { getActivities } from '../../redux/activities/selectors';
import { getWorkouts } from '../../redux/workout/selectors';
import * as ColorUtil from '../../utils/ColorUtil';
import * as FontUtil from '../../utils/FontUtil';
import * as ActivityUtil from '../../utils/ActivityUtil';
import StatusBarSpacer from '../../components/status-bar-spacer/StatusBarSpacer';
import ActivityGroupCard from '../../components/cards/activity-group-card/ActivityGroupCard';
import ActivityTimestamp from '../../components/activity-timestamp/ActivityTimestamp';

import type {
  Dispatch,
  ActivityCreate,
  AppState,
  Workout,
  Activity,
} from '../../types';

type OwnProps = {
  navigator: Navigator,
  activityNameSearch: string,
};

type StateProps = {
  workouts: Workout[],
  activities: Activity[],
};

type DispatchProps = {
  loadActivities: () => Promise<any>,
  createActivity: (create: ActivityCreate) => Promise<any>,
};

type Props = OwnProps & StateProps & DispatchProps;

type State = {
  backgroundAnim: Animated.Value,
  scrollViewContentOffsetY: Animated.Value,
};

const AnimatedIcon = Animated.createAnimatedComponent(EvilIcons);

const styles = {
  backgroundContainer: {
    flex: 1,
  },
  background: (anim: Animated.Value) => ({
    ...StyleSheet.absoluteFillObject,
    opacity: anim,
    backgroundColor: ColorUtil.hexToRgbaString(UI_COLORS.DARK_GREY, BACKGROUND_LAYER_OPACITY),
  }),
  statusBar: (scrollViewContentOffsetY: Animated.Value) => ({
    backgroundColor: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [
        ColorUtil.hexToRgbaString(UI_COLORS.DARK_GREY, 0),
        ColorUtil.hexToRgbaString(UI_COLORS.DARK_GREY, 0),
        ColorUtil.hexToRgbaString(UI_COLORS.DARK_GREY, 1),
        ColorUtil.hexToRgbaString(UI_COLORS.DARK_GREY, 1),
      ],
    }),
  }),
  foregroundContainer: (scrollViewContentOffsetY: Animated.Value) => ({
    flex: 1,
    marginBottom: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [45, 45, 0, 0],
    }),
    marginTop: 15,
    marginHorizontal: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [15, 15, 0, 0],
    }),
    backgroundColor: UI_COLORS.OFF_WHITE,
    shadowColor: UI_COLORS.DARK_GREY,
    shadowRadius: 55,
    shadowOpacity: 0.25,
    shadowOffset: {
      height: 4,
      width: 3,
    },
    borderRadius: 6,
    paddingHorizontal: 15,
    paddingVertical: 7,
  }),
  scrollView: {
    flex: 1,
  },
  scrollViewHeader: (scrollViewContentOffsetY: Animated.Value) => ({
    height: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [100, 100, 75, 75],
    }),
    backgroundColor: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [
        ColorUtil.hexToRgbaString(UI_COLORS.WHITE, 0),
        ColorUtil.hexToRgbaString(UI_COLORS.WHITE, 0),
        ColorUtil.hexToRgbaString(UI_COLORS.WHITE, 1),
        ColorUtil.hexToRgbaString(UI_COLORS.WHITE, 1),
      ],
    }),
    shadowColor: UI_COLORS.DARK_GREY,
    shadowRadius: 35,
    shadowOpacity: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [0, 0, 0.15, 0.15],
    }),
    shadowOffset: {
      height: 4,
      width: 3,
    },
  }),
  scrollViewHeaderInner: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  dismissModalButton: {},
  dismissModalIcon: (scrollViewContentOffsetY: Animated.Value) => ({
    color: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [
        TEXT_COLORS.OFF_WHITE,
        TEXT_COLORS.OFF_WHITE,
        UI_COLORS.DARK_GREY,
        UI_COLORS.DARK_GREY,
      ],
    }),
    fontSize: 35,
  }),
  activityNameText: (scrollViewContentOffsetY: Animated.Value) => ({
    ...FontUtil.getFontStyle('title'),
    fontSize: 31,
    fontWeight: 'normal',
    color: scrollViewContentOffsetY.interpolate({
      inputRange: [-Infinity, 0, 100, Infinity],
      outputRange: [
        UI_COLORS.WHITE,
        UI_COLORS.WHITE,
        UI_COLORS.DARK_GREY,
        UI_COLORS.DARK_GREY,
      ],
    }),
  }),
  activityCard: {
    marginVertical: 8,
    borderRadius: 6,
    shadowColor: UI_COLORS.DARK_GREY,
    shadowOpacity: 0.03,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 4,
    },
    backgroundColor: UI_COLORS.WHITE,
    paddingHorizontal: 17,
    paddingTop: 13,
    paddingBottom: 12,
    alignItems: 'flex-start',
  },
};

function mapStateToProps(state: AppState): StateProps {
  return {
    workouts: getWorkouts(state),
    activities: getActivities(state),
  };
}

function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
  return {
    loadActivities: () => dispatch(loadActivities()),
    createActivity: (create: ActivityCreate) =>
      dispatch(createActivity(create)),
  };
}

@connect(mapStateToProps, mapDispatchToProps)
@autobind
export default class ActivityModal extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      backgroundAnim: new Animated.Value(0),
      scrollViewContentOffsetY: new Animated.Value(0),
    };
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
  }

  async componentWillMount() {
    await this.props.loadActivities();
  }

  componentDidMount() {
    this.animateIn();
  }

  onNavigatorEvent(event: any) {
    if (event.id === 'willDisappear') {
      this.animateOut();
    }
  }

  animateIn() {
    Animated.timing(this.state.backgroundAnim, {
      delay: 400,
      toValue: 1,
      duration: 350,
    }).start();
  }

  animateOut() {
    Animated.timing(this.state.backgroundAnim, {
      toValue: 0,
      duration: 150,
    }).start();
  }

  dismissModal() {
    this.props.navigator.dismissModal({ animationType: 'slide-down' });
  }

  handleScrollViewOnScroll() {
    return Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: {
              y: this.state.scrollViewContentOffsetY,
            },
          },
        },
      ],
      { useNativeDriver: false }
    );
  }

  render() {
    return (
      <View style={styles.backgroundContainer}>
        <Animated.View style={styles.background(this.state.backgroundAnim)} />
        <Animated.ScrollView
          style={styles.scrollView}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          scrollEventThrottle={16}
          onScroll={this.handleScrollViewOnScroll()}
          overScrollMode="always"
          stickyHeaderIndices={[0]}
        >
          <View>
            <Animated.View
              style={styles.scrollViewHeader(
                this.state.scrollViewContentOffsetY
              )}
            >
              <StatusBarSpacer
                style={styles.statusBar(this.state.scrollViewContentOffsetY)}
              />
              <View style={styles.scrollViewHeaderInner}>
                <Animated.Text
                  style={styles.activityNameText(
                    this.state.scrollViewContentOffsetY
                  )}
                >
                  {this.props.activityNameSearch}
                </Animated.Text>
                <TouchableOpacity
                  style={styles.dismissModalButton}
                  onPress={this.dismissModal}
                >
                  <AnimatedIcon
                    name="close"
                    style={styles.dismissModalIcon(
                      this.state.scrollViewContentOffsetY
                    )}
                  />
                </TouchableOpacity>
              </View>
            </Animated.View>
          </View>
          <Animated.View
            style={styles.foregroundContainer(
              this.state.scrollViewContentOffsetY
            )}
          >
            {this.renderActivities()}
          </Animated.View>
        </Animated.ScrollView>
      </View>
    );
  }

  renderActivities() {
    const activities = sortActivities(
      this.props.activities,
      this.props.activityNameSearch
    );
    return map(activities, (activities, workoutId) => {
      const activity: Activity = first(activities);
      return (
        <View key={workoutId} style={styles.activityCard}>
          <ActivityTimestamp timestamp={activity.eventTimestamp} />
          <ActivityGroupCard
            activities={activities}
            onRequestUpdateWorkout={() => {}}
            onRequestLogActivity={() => {}}
          />
        </View>
      );
    });
  }
}

function sortActivities(
  activities: Activity[],
  displayName: string
): { [key: string]: Activity[] } {
  const activitiesByDisplayName = activities.filter(
    a => a.displayName === displayName
  );
  return ActivityUtil.groupActivitiesByWorkout(activitiesByDisplayName);
}
