// @flow
import { Navigation } from 'react-native-navigation';
import HomeScreen from './home-screen/HomeScreen';
import LogActivityScreen from './log-activity-screen/LogActivityScreen';
import ActivityModal from './activity-modal/ActivityModal';
import CalendarModal from './calendar-modal/CalendarModal';

export function registerScreens(store: any, Provider: React$ElementType) {
  Navigation.registerComponent(
    'gymlog.HomeScreen',
    () => HomeScreen,
    store,
    Provider
  );
  Navigation.registerComponent(
    'gymlog.LogActivityScreen',
    () => LogActivityScreen,
    store,
    Provider
  );
  Navigation.registerComponent(
    'gymlog.ActivityModal',
    () => ActivityModal,
    store,
    Provider
  );
  Navigation.registerComponent(
    'gymlog.CalendarModal',
    () => CalendarModal,
    store,
    Provider
  );
}
