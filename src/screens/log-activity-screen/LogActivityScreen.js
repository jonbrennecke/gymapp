// @flow
import React, { Component } from 'react';
import { View } from 'react-native';
import { Navigator } from 'react-native-navigation';
import { autobind } from 'core-decorators';
import { connect } from 'react-redux';
import first from 'lodash/first';

import * as Debug from '../../utils/DebugUtil';
import { UI_COLORS, SCREEN_STYLES } from '../../constants';
import ActivityForm from '../../components/activity-form/ActivityForm';
import { createActivity } from '../../redux/activities/actionCreators';
import { getWorkouts } from '../../redux/workout/selectors';

import type {
  Dispatch,
  ActivityCreate,
  ActivityMeasurement,
  AppState,
  Workout,
} from '../../types';

type OwnProps = {
  navigator: Navigator,
};

type StateProps = {
  workouts: Workout[],
};

type DispatchProps = {
  createActivity: (create: ActivityCreate) => Promise<any>,
};

type Props = OwnProps & StateProps & DispatchProps;

const styles = {
  container: {
    flex: 1,
    backgroundColor: UI_COLORS.OFF_WHITE,
  },
};

function mapStateToProps(state: AppState): StateProps {
  return {
    workouts: getWorkouts(state),
  };
}

function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
  return {
    createActivity: (create: ActivityCreate) =>
      dispatch(createActivity(create)),
  };
}

@connect(mapStateToProps, mapDispatchToProps)
@autobind
export default class LogActivityScreen extends Component<Props, {}> {
  componentWillMount() {
    this.props.navigator.setStyle(SCREEN_STYLES.NAVBAR);
  }

  async saveActivity(activity: {
    displayName: string,
    eventTimestamp: string,
    measurement: ActivityMeasurement,
    repititions: number,
  }) {
    const activeWorkouts = this.props.workouts.filter(w => !w.endedTimestamp);
    const activeWorkout = first(activeWorkouts);
    if (!activeWorkout) {
      Debug.logErrorMessage(
        'LogActivityScreen.saveActivity called without an active workout'
      );
      return;
    }
    await this.props.createActivity({
      ...activity,
      workout: activeWorkout.id,
    });
    this.props.navigator.pop();
  }

  render() {
    const activeWorkouts = this.props.workouts.filter(w => !w.endedTimestamp);
    const activeWorkout = first(activeWorkouts);
    if (!activeWorkout) {
      Debug.logErrorMessage(
        'LogActivityScreen.saveActivity called without an active workout'
      );
      return;
    }
    return (
      <View style={styles.container}>
        <ActivityForm
          workoutTimestamp={activeWorkout.startedTimestamp}
          onRequestSaveActivity={this.saveActivity}
        />
      </View>
    );
  }
}
