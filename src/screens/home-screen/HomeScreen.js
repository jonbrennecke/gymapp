// @flow
import React, { Component } from 'react';
import { Animated, View } from 'react-native';
import { autobind } from 'core-decorators';
import { Navigator } from 'react-native-navigation';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import { BoxShadow } from 'react-native-shadow';
import first from 'lodash/first';

import { loadActivities } from '../../redux/activities/actionCreators';
import { getActivities } from '../../redux/activities/selectors';
import { getWorkouts } from '../../redux/workout/selectors';
import {
  updateWorkout,
  endActiveWorkouts,
  deleteActiveWorkouts,
  loadWorkouts,
  startWorkout,
} from '../../redux/workout/actionCreators';
import { UI_COLORS, SCREENS, SCREEN_WIDTH } from '../../constants';
import * as DateTimeUtil from '../../utils/DateTimeUtil';
import CardList from '../../components/card-list/CardList';
import DateSelector from '../../components/date-selector/DateSelector';
import StatusBarSpacer from '../../components/status-bar-spacer/StatusBarSpacer';
import FloatingActionButton from '../../components/floating-action-button/FloatingActionButton';
import FloatingActionButtonContainer from '../../components/floating-action-button-container/FloatingActionButtonContainer';
import StatsHeader from '../../components/stats-header/StatsHeader';

import type { Dispatch, AppState, Activity, Workout } from '../../types';

type OwnProps = {
  navigator: Navigator,
};

type StateProps = {
  workouts: Workout[],
  activities: Activity[],
};

type DispatchProps = {
  loadActivities: () => Promise<any>,
  loadWorkouts: () => Promise<any>,
  startWorkout: (startedTimestamp?: string) => Promise<void>,
  endActiveWorkouts: () => Promise<any>,
  deleteActiveWorkouts: () => Promise<any>,
  updateWorkout: (workout: Workout) => Promise<any>,
};

type Props = OwnProps & StateProps & DispatchProps;

type State = {
  currentTimestamp: string,
  scrollViewContentOffsetY: Animated.Value,
};

const styles = {
  container: {
    flex: 1,
    backgroundColor: UI_COLORS.OFF_WHITE,
  },
  scrollView: {
    flex: 1,
  },
  activityForm: {
    marginBottom: 15,
  },
  workoutStartButton: {
    marginVertical: 17,
  },
  header: {
    flex: 1,
    height: 200,
    justifyContent: 'center',
  },
  headerGradient: {
    position: 'absolute',
    height: 425,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  workoutList: {
    flex: 1,
    minHeight: 300,
    backgroundColor: UI_COLORS.OFF_WHITE,
  },
  headerBoxShadow: {
    width: SCREEN_WIDTH,
    height: 0,
    color: UI_COLORS.DARK_GREY,
    border: 25,
    opacity: 0.05,
    x: 0,
    y: 0,
    style: {
      position: 'absolute',
      bottom: 0,
    },
  },
  dateSelector: {
    marginTop: 15,
  },
  headerBackground: {
    overflow: 'hidden',
    position: 'absolute',
    height: 200,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: -1,
  },
};

function mapStateToProps(state: AppState): StateProps {
  return {
    activities: getActivities(state),
    workouts: getWorkouts(state),
  };
}

function mapDispatchToProps(dispatch: Dispatch<any>): DispatchProps {
  return {
    loadActivities: () => dispatch(loadActivities()),
    loadWorkouts: () => dispatch(loadWorkouts()),
    startWorkout: (startedTimestamp?: string) =>
      dispatch(startWorkout(startedTimestamp)),
    endActiveWorkouts: () => dispatch(endActiveWorkouts()),
    deleteActiveWorkouts: () => dispatch(deleteActiveWorkouts()),
    updateWorkout: (w: Workout) => dispatch(updateWorkout(w)),
  };
}

@connect(mapStateToProps, mapDispatchToProps)
@autobind
export default class HomeScreen extends Component<Props, State> {
  state: State = {
    currentTimestamp: DateTimeUtil.timestamp(),
    scrollViewContentOffsetY: new Animated.Value(1),
  };

  async componentWillMount() {
    await this.props.loadActivities();
    await this.props.loadWorkouts();
  }

  async startWorkout() {
    await this.props.startWorkout(this.state.currentTimestamp);
  }

  async endWorkout() {
    this.props.navigator.pop();
    await this.props.endActiveWorkouts();
  }

  async quitWorkout() {
    await this.props.deleteActiveWorkouts();
    this.props.navigator.pop();
  }

  async pushLogActivityScreen() {
    const activeWorkouts = this.props.workouts.filter(w => !w.endedTimestamp);
    const hasActiveWorkout = !!first(activeWorkouts);
    if (!hasActiveWorkout) {
      await this.startWorkout();
    }
    this.props.navigator.push(SCREENS.LOG_ACTIVITY_SCREEN);
  }

  showActivityModal(activityNameSearch: string) {
    this.props.navigator.showModal({
      ...SCREENS.ACTIVITY_MODAL,
      passProps: {
        activityNameSearch,
      },
    });
  }

  showCalendarModal() {
    this.props.navigator.showModal({
      ...SCREENS.CALENDAR_MODAL,
      passProps: {
        onRequestChangeDay: (currentTimestamp) => {
          this.setState({
            currentTimestamp
          });
          this.props.navigator.dismissModal();
        },
        onRequestSelectNextMonth: this.selectNextMonth,
        onRequestSelectPreviousMonth: this.selectPreviousMonth,
      },
    });
  }

  handleScrollViewOnScroll() {
    return Animated.event(
      [
        {
          nativeEvent: {
            contentOffset: {
              y: this.state.scrollViewContentOffsetY,
            },
          },
        },
      ],
      { useNativeDriver: true }
    );
  }

  selectPreviousDate() {
    this.setState({
      currentTimestamp: DateTimeUtil.subtractDays(this.state.currentTimestamp),
    });
  }

  selectNextDate() {
    this.setState({
      currentTimestamp: DateTimeUtil.addDays(this.state.currentTimestamp),
    });
  }

  selectPreviousMonth() {
    this.setState({
      currentTimestamp: DateTimeUtil.subtractMonths(
        this.state.currentTimestamp
      ),
    });
  }

  selectNextMonth() {
    this.setState({
      currentTimestamp: DateTimeUtil.addMonths(this.state.currentTimestamp),
    });
  }

  render() {
    const activeWorkouts = this.props.workouts.filter(w => !w.endedTimestamp);
    const hasActiveWorkout = !!first(activeWorkouts);
    return (
      <View style={styles.container}>
        <StatusBarSpacer />
        <LinearGradient
          start={{ x: 1.0, y: 0.5 }}
          end={{ x: 0.0, y: 0.5 }}
          colors={[UI_COLORS.MEDIUM_GREEN, UI_COLORS.LIGHT_GREEN]}
          style={styles.headerGradient}
        />
        <Animated.ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          style={styles.scrollView}
          scrollEventThrottle={16}
          onScroll={this.handleScrollViewOnScroll}
          overScrollMode="always"
        >
          {/* TODO this should be encapsulated by a "HomeScreenHeader" component */}
          <View style={styles.header}>
            <StatsHeader activities={this.props.activities} />
            <View style={styles.headerBackground}>
              <BoxShadow setting={styles.headerBoxShadow} />
            </View>
          </View>

          <View style={styles.workoutList}>
            <DateSelector
              style={styles.dateSelector}
              date={this.state.currentTimestamp}
              formatDate={DateTimeUtil.formatCalendarDate}
              onRequestSelectPreviousDate={this.selectPreviousDate}
              onRequestSelectNextDate={this.selectNextDate}
              onPressCurrentDate={this.showCalendarModal}
            />
            <CardList
              date={this.state.currentTimestamp}
              workouts={this.props.workouts}
              activities={this.props.activities}
              onRequestUpdateWorkout={workout =>
                this.props.updateWorkout(workout)
              }
              onRequestLogActivity={this.pushLogActivityScreen}
              onRequestShowActivityModal={this.showActivityModal}
            />
          </View>
        </Animated.ScrollView>
        <FloatingActionButtonContainer>
          <FloatingActionButton
            visible={hasActiveWorkout}
            type="stop"
            size="small"
            onPress={this.endWorkout}
          />
          <FloatingActionButton
            type={hasActiveWorkout ? 'log' : 'play'}
            size="large"
            onPress={() => {
              if (hasActiveWorkout) {
                this.pushLogActivityScreen();
                return;
              }
              this.startWorkout();
            }}
          />
        </FloatingActionButtonContainer>
      </View>
    );
  }
}
