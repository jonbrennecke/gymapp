// @flow 
import React, { Component } from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { autobind } from 'core-decorators';
import { BoxShadow } from 'react-native-shadow';
import { Navigator } from 'react-native-navigation';

import Calendar from '../../components/calendar/Calendar';
import StatusBarSpacer from '../../components/status-bar-spacer/StatusBarSpacer';
import { getWorkouts } from '../../redux/workout/selectors';
import { loadWorkouts } from '../../redux/workout/actionCreators';
import { hexToRgbaString } from '../../utils/ColorUtil';
import { UI_COLORS, SCREEN_HEIGHT, SCREEN_WIDTH, BACKGROUND_LAYER_OPACITY } from '../../constants';

import type { AppState, Dispatch, Workout } from '../../types';

type OwnProps = {
  navigator: Navigator,
  currentTimestamp: string,
  onRequestChangeDay: (currentTimestamp: string) => any,
  onRequestSelectNextMonth: () => any,
  onRequestSelectPreviousMonth: () => any,
}; 

type StateProps = {
  workouts: Workout[],
};

type DispatchProps = {};

type Props = OwnProps & StateProps & DispatchProps;

const MODAL_HEIGHT = SCREEN_HEIGHT - 250;

const styles = {
  container: {
    flex: 1,
  },
  modal: {
    flex: 1,
    height: MODAL_HEIGHT,
    backgroundColor: UI_COLORS.WHITE,
  },
  backgroundTouchable: {
    flex: 1,
    backgroundColor: hexToRgbaString(UI_COLORS.DARK_GREY, BACKGROUND_LAYER_OPACITY),
  },
  statusBar: {
    backgroundColor: UI_COLORS.DARK_GREY,
  }
};

const BOX_SHADOW_PARAMS = {
  width: SCREEN_WIDTH,
  height: MODAL_HEIGHT,
  color: '#000',
  border: 100,
  opacity: 0.15,
  x: 0,
  y: 0,
};

function mapStateToProps(state: AppState): StateProps {
  return {
    workouts: getWorkouts(state),
  };
}

function mapDispatchToProps(dispatch: Dispatch<*>): DispatchProps {
  return {
    loadWorkouts: () => dispatch(loadWorkouts()),
  };
}

@connect(mapStateToProps, mapDispatchToProps)
@autobind
export default class CalendarModal extends Component<Props, {}> {

  closeModal() {
    // TODO animate out
    this.props.navigator.dismissModal({
      animationType: 'none',
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <BoxShadow setting={BOX_SHADOW_PARAMS}>
          <View style={styles.modal}>
            <StatusBarSpacer style={styles.statusBar}/>
            <Calendar
              workouts={this.props.workouts}
              monthTimestamp={this.props.currentTimestamp}
              onRequestSelectPreviousMonth={this.props.onRequestSelectPreviousMonth}
              onRequestSelectNextMonth={this.props.onRequestSelectNextMonth}
              onRequestChangeDay={this.props.onRequestChangeDay}
            />
          </View>
        </BoxShadow>
        <TouchableWithoutFeedback onPress={this.closeModal}>
          <View style={styles.backgroundTouchable} />
        </TouchableWithoutFeedback>
      </View>
    );
  }
}