// @flow
import type { Activity, AppState } from '../../types';

export function getActivities(state: AppState): Activity[] {
  return state.activityState.activities;
}
