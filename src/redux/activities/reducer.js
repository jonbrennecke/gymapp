// @flow
import { handleActions } from 'redux-actions';
import uniqBy from 'lodash/uniqBy';
import { ACTION_TYPES } from './constants';

import type {
  ActivityState,
  Activity,
  ReceiveActivityPayload,
  ReceiveActivitiesPayload,
  Action,
} from '../../types';

const initialState: ActivityState = {
  activities: [],
};

const actions = {
  [ACTION_TYPES.RECEIVE_CREATED_ACTIVITY]: receiveActivity,
  [ACTION_TYPES.RECEIVE_LOADED_ACTIVITIES]: receiveActivities,
};

function receiveActivity(
  state: ActivityState,
  { payload }: Action<ReceiveActivityPayload>
): ActivityState {
  if (!payload) {
    return state;
  }
  return addActivity(state, payload.activity);
}

function receiveActivities(
  state: ActivityState,
  { payload }: Action<ReceiveActivitiesPayload>
): ActivityState {
  if (!payload) {
    return state;
  }
  return addActivities(state, payload.activities);
}

function addActivity(
  { activities, ...state }: ActivityState,
  newActivity: Activity
): ActivityState {
  return {
    ...state,
    activities: uniqBy([newActivity, ...activities], 'id'),
  };
}

function addActivities(
  { activities, ...state }: ActivityState,
  newActivities: Activity[]
): ActivityState {
  return {
    ...state,
    activities: uniqBy([...newActivities, ...activities], 'id'),
  };
}

export default handleActions(actions, initialState);
