// @flow
import { ACTION_TYPES } from './constants';
import * as api from './api';
import * as Debug from '../../utils/DebugUtil';

import type { ActivityCreate, Dispatch } from '../../types';

export const createActivity = (create: ActivityCreate) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ACTION_TYPES.CREATE_ACTIVITY });
    try {
      const activity = await api.createActivity(create);
      dispatch({
        type: ACTION_TYPES.RECEIVE_CREATED_ACTIVITY,
        payload: {
          activity,
        },
      });
    } catch (error) {
      await Debug.logError(error);
    }
  };
};

export const loadActivities = () => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ACTION_TYPES.LOAD_ACTIVITIES });
    try {
      const activities = await api.loadActivities();
      dispatch({
        type: ACTION_TYPES.RECEIVE_LOADED_ACTIVITIES,
        payload: {
          activities,
        },
      });
    } catch (error) {
      await Debug.logError(error);
    }
  };
};
