// @flow
// eslint-disable-next-line import/default
import DeviceInfo from 'react-native-device-info';
import uuid from 'uuid';
import { getRealm } from '../realm';
import ActivitySchema from './schema';
import { timestamp } from '../../utils/DateTimeUtil';

import type { Id, Activity, ActivityCreate, RealmActivity } from '../../types';

export const createActivity = async (
  create: ActivityCreate
): Promise<Activity> => {
  const realm = await getRealm();
  let activity: ?RealmActivity = null;
  realm.write(() => {
    activity = realm.create(ActivitySchema.name, {
      id: uuid.v4(),
      uuid: uuid.v4(),
      createdBy: DeviceInfo.getDeviceId(),
      createdTimestamp: timestamp(),
      updatedTimestamp: timestamp(),
      displayName: create.displayName,
      workout: create.workout,
      eventTimestamp: create.eventTimestamp,
      measurementTag: create.measurement.tag,
      measurementAmount: create.measurement.amount,
      measurementUnit: create.measurement.unit,
      repititions: create.repititions,
    });
  });
  if (!activity) {
    throw new Error('Failed to create activity');
  }
  return realmActivityToJS(activity);
};

function realmActivityToJS(activity: RealmActivity): Activity {
  return {
    id: activity.id,
    uuid: activity.uuid,
    createdBy: activity.createdBy,
    createdTimestamp: activity.createdTimestamp,
    updatedTimestamp: activity.updatedTimestamp,
    displayName: activity.displayName,
    workout: activity.workout,
    eventTimestamp: activity.eventTimestamp,
    // $FlowFixMe
    measurement: {
      tag: activity.measurementTag,
      amount: activity.measurementAmount,
      unit: activity.measurementUnit,
    },
    repititions: activity.repititions,
  };
}

export const loadActivities = async (filter?: string) => {
  const realm = await getRealm();
  const activities = realm.objects(ActivitySchema.name);
  if (filter) {
    return activities.filtered(filter).map(realmActivityToJS);
  }
  return activities.map(realmActivityToJS);
};

export const deleteActivity = async (id: Id) => {
  const realm = await getRealm();
  const activity = realm.objects(ActivitySchema.name).filter(`id == ${id}`)
    .first;
  await realm.delete(activity);
};

export const updateActivity = async ({ id, ...update }: Activity) => {
  const realm = await getRealm();
  const activity = realm.objectForPrimaryKey(ActivitySchema.name, id);
  realm.write(() => {
    activity.createdBy = update.createdBy;
    activity.createdTimestamp = update.createdTimestamp;
    activity.updatedTimestamp = timestamp();
    activity.displayName = update.displayName;
    activity.workout = update.workout;
    activity.eventTimestamp = update.eventTimestamp;
    activity.measurementTag = update.measurement.tag;
    activity.measurementAmount = update.measurement.amount;
    activity.measurementUnit = update.measurement.unit;
    activity.repititions = update.repititions;
  });
  return realmActivityToJS(activity);
};
