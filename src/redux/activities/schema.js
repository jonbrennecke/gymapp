// @flow
const ActivitySchema = {
  name: 'Activity',
  primaryKey: 'id',
  properties: {
    id: 'string',
    uuid: 'string',
    createdBy: 'string',
    createdTimestamp: 'string',
    updatedTimestamp: 'string',
    displayName: 'string',
    workout: 'string',
    eventTimestamp: 'string',
    measurementTag: 'string',
    measurementAmount: 'double',
    measurementUnit: 'string',
    repititions: 'int',
  },
};

export default ActivitySchema;
