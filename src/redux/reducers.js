// @flow
import workoutsReducer from './workout/reducer';
import activityReducer from './activities/reducer';

export default {
  workoutState: workoutsReducer,
  activityState: activityReducer,
};
