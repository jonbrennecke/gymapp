// @flow
import Promise from 'bluebird';
import * as api from './api';
import { timestamp } from '../../utils/DateTimeUtil';
import * as Debug from '../../utils/DebugUtil';
import { ACTION_TYPES } from './constants';

import type { Dispatch, Workout } from '../../types';

export const startWorkout = (startedTimestamp?: string = timestamp()) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ACTION_TYPES.START_WORKOUT });
    try {
      const workout = await api.createWorkout({ startedTimestamp });
      dispatch({
        type: ACTION_TYPES.RECEIVE_STARTED_WORKOUT,
        payload: {
          workout,
        },
      });
    } catch (error) {
      await Debug.logError(error);
    }
  };
};

export const updateWorkout = (workout: Workout) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ACTION_TYPES.UPDATE_WORKOUT });
    try {
      const updatedWorkout = await api.updateWorkout(workout);
      dispatch({
        type: ACTION_TYPES.RECEIVE_UPDATED_WORKOUT,
        payload: {
          workout: updatedWorkout,
        },
      });
    } catch (error) {
      await Debug.logError(error);
    }
  };
};

export const endActiveWorkouts = () => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ACTION_TYPES.END_WORKOUTS });
    try {
      const workouts = await api.loadWorkouts('endedTimestamp == null');
      const updatedWorkouts = await Promise.map(workouts, async workout => {
        return await api.updateWorkout({
          ...workout,
          endedTimestamp: timestamp(),
        });
      });
      dispatch({
        type: ACTION_TYPES.RECEIVE_ENDED_WORKOUTS,
        payload: {
          workouts: updatedWorkouts,
        },
      });
    } catch (error) {
      await Debug.logError(error);
    }
  };
};

export const deleteActiveWorkouts = () => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ACTION_TYPES.DELETE_ACTIVE_WORKOUTS });
    try {
      const workouts = await api.loadWorkouts('endedTimestamp == null');
      await Promise.map(workouts, async workout => {
        return await api.deleteWorkout(workout.id);
      });
      await dispatch(loadWorkouts());
    } catch (error) {
      await Debug.logError(error);
    }
  };
};

export const loadWorkouts = () => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ACTION_TYPES.LOAD_WORKOUTS });
    try {
      const workouts = await api.loadWorkouts();
      dispatch({
        type: ACTION_TYPES.RECEIVE_LOADED_WORKOUTS,
        payload: {
          workouts,
        },
      });
    } catch (error) {
      await Debug.logError(error);
    }
  };
};
