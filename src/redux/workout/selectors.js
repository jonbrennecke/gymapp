// @flow
import values from 'lodash/values';

import type { Workout, AppState } from '../../types';

export function getWorkouts(state: AppState): Workout[] {
  return values(state.workoutState.workouts);
}
