// @flow
import { handleActions } from 'redux-actions';
import keyBy from 'lodash/keyBy';
import values from 'lodash/values';
import { ACTION_TYPES } from './constants';

import type {
  WorkoutState,
  Workout,
  ReceiveWorkoutPayload,
  ReceiveWorkoutsPayload,
  Action,
} from '../../types';

const initialState: WorkoutState = {
  workouts: {},
};

const actions = {
  [ACTION_TYPES.RECEIVE_STARTED_WORKOUT]: receiveWorkout,
  [ACTION_TYPES.RECEIVE_ENDED_WORKOUTS]: receiveWorkouts,
  [ACTION_TYPES.RECEIVE_LOADED_WORKOUTS]: receiveWorkouts,
  [ACTION_TYPES.RECEIVE_UPDATED_WORKOUT]: receiveWorkout,
};

function receiveWorkout(
  state: WorkoutState,
  { payload }: Action<ReceiveWorkoutPayload>
): WorkoutState {
  if (!payload) {
    return state;
  }
  return addWorkout(state, payload.workout);
}

function receiveWorkouts(
  state: WorkoutState,
  { payload }: Action<ReceiveWorkoutsPayload>
): WorkoutState {
  if (!payload) {
    return state;
  }
  return addWorkouts(state, payload.workouts);
}

function addWorkout(
  { workouts: oldWorkouts, ...state }: WorkoutState,
  workout: Workout
): WorkoutState {
  const workouts = [...values(oldWorkouts), workout];
  return {
    ...state,
    workouts: keyBy(workouts, 'id'),
  };
}

function addWorkouts(
  { workouts: oldWorkouts, ...state }: WorkoutState,
  newWorkouts: Workout[]
): WorkoutState {
  const workouts = [...values(oldWorkouts), ...newWorkouts];
  return {
    ...state,
    workouts: keyBy(workouts, 'id'),
  };
}

export default handleActions(actions, initialState);
