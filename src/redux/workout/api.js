// @flow
// eslint-disable-next-line import/default
import DeviceInfo from 'react-native-device-info';
import uuid from 'uuid';
import { getRealm } from '../realm';
import WorkoutSchema from './schema';
import { timestamp } from '../../utils/DateTimeUtil';

import type { Id, Workout, WorkoutCreate, RealmWorkout } from '../../types';

export const createWorkout = async ({
  startedTimestamp,
}: WorkoutCreate): Promise<Workout> => {
  const realm = await getRealm();
  let workout: ?RealmWorkout = null;
  realm.write(() => {
    workout = realm.create(WorkoutSchema.name, {
      id: uuid.v4(),
      uuid: uuid.v4(),
      createdBy: DeviceInfo.getDeviceId(),
      createdTimestamp: timestamp(),
      updatedTimestamp: timestamp(),
      startedTimestamp: startedTimestamp,
      endedTimestamp: null,
      displayName: null,
    });
  });
  if (!workout) {
    throw new Error('Failed to create workout');
  }
  return realmWorkoutToJS(workout);
};

// TODO: could use _.pick here
function realmWorkoutToJS(workout: RealmWorkout): Workout {
  return {
    id: workout.id,
    uuid: workout.uuid,
    createdBy: workout.createdBy,
    createdTimestamp: workout.createdTimestamp,
    updatedTimestamp: workout.updatedTimestamp,
    startedTimestamp: workout.startedTimestamp,
    endedTimestamp: workout.endedTimestamp,
    displayName: workout.displayName,
  };
}

export const updateWorkout = async ({ id, ...update }: Workout) => {
  const realm = await getRealm();
  const workout = realm.objectForPrimaryKey(WorkoutSchema.name, id);
  realm.write(() => {
    workout.createdBy = update.createdBy;
    workout.createdTimestamp = update.createdTimestamp;
    workout.updatedTimestamp = timestamp();
    workout.startedTimestamp = update.startedTimestamp;
    workout.endedTimestamp = update.endedTimestamp;
    workout.displayName = update.displayName;
  });
  return realmWorkoutToJS(workout);
};

export const loadWorkouts = async (filter?: string) => {
  const realm = await getRealm();
  const workouts = realm.objects(WorkoutSchema.name);
  if (filter) {
    return workouts.filtered(filter).map(realmWorkoutToJS);
  }
  return workouts.map(realmWorkoutToJS);
};

export const deleteWorkout = async (id: Id) => {
  const realm = await getRealm();
  const workout = realm.objectForPrimaryKey(WorkoutSchema.name, id);
  realm.write(() => {
    realm.delete(workout);
  });
};
