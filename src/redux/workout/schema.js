// @flow
const WorkoutSchema = {
  name: 'Workout',
  primaryKey: 'id',
  properties: {
    id: 'string',
    uuid: 'string',
    createdBy: 'string',
    createdTimestamp: 'string',
    updatedTimestamp: 'string',
    startedTimestamp: 'string',
    endedTimestamp: 'string?',
    displayName: 'string?',
  },
};

export default WorkoutSchema;
