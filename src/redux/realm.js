// @flow
import Realm from 'realm';
import ActivitySchema from './activities/schema';
import WorkoutSchema from './workout/schema';

const REALM_SCHEMAS = [ActivitySchema, WorkoutSchema];

let realm: ?Realm = null;

export const getRealm = async (): Realm => {
  if (!realm) {
    realm = await Realm.open({
      schema: REALM_SCHEMAS,
      schemaVersion: 7,
    });
  }
  return realm;
};
