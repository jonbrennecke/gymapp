// @flow
import React, { Component } from 'react';
import { View } from 'react-native';

import * as CardUtil from '../../utils/CardUtil';
import CallToActionCard from '../cards/call-to-action-card/CallToActionCard';
import WorkoutCard from '../cards/workout-card/WorkoutCard';

import type { Style, Workout, Activity } from '../../types';

type Props = {
  style?: Style | Style[],
  date: string,
  workouts: Workout[],
  activities: Activity[],
  onRequestUpdateWorkout: (workout: Workout) => any,
  onRequestLogActivity: () => any,
  onRequestShowActivityModal: (activityNameSearch: string) => any,
};

const styles = {
  container: {
    flex: 1,
    paddingHorizontal: 10,
  },
};

export default class CardList extends Component<Props, {}> {
  render() {
    const cards = CardUtil.getCards(
      this.props.workouts,
      this.props.activities,
      this.props.date
    );
    return (
      <View style={[styles.container, this.props.style]}>
        {cards.map(params => {
          switch (params.tag) {
            case 'workout':
              return (
                <WorkoutCard
                  key={params.workout.id}
                  workout={params.workout}
                  activities={params.activities}
                  onRequestLogActivity={this.props.onRequestLogActivity}
                  onRequestUpdateWorkout={this.props.onRequestUpdateWorkout}
                  onRequestShowActivityModal={
                    this.props.onRequestShowActivityModal
                  }
                />
              );
            case 'call-to-action':
              return (
                <CallToActionCard
                  key="call-to-action"
                  date={this.props.date}
                  lastWorkoutTimestamp={params.lastWorkoutTimestamp}
                  onRequestLogActivity={this.props.onRequestLogActivity}
                />
              );
            default:
              break;
          }
        })}
      </View>
    );
  }
}
