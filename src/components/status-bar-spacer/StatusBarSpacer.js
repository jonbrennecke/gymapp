// @flow
import React, { Component } from 'react';
import { Animated, Platform } from 'react-native';

import {
  INITIAL_STATUS_BAR_HEIGHT,
  IOS_STATUS_BAR_HEIGHT,
} from '../../constants';

import type { Style } from '../../types';

type Props = {
  style?: Style | Style[],
};

type State = {
  statusBarHeight: number,
};

const styles = {
  statusBar: (statusBarHeight: number) => ({
    height: statusBarHeight,
  }),
};

export default class StatusBarSpacer extends Component<Props, State> {
  state = {
    statusBarHeight: INITIAL_STATUS_BAR_HEIGHT,
  };

  componentDidMount() {
    // FIXME:
    // let { currentHeight } = StatusBar;
    // if (!currentHeight) {
    //   currentHeight =
    //     Platform.OS === 'ios' ? IOS_STATUS_BAR_HEIGHT : StatusBarManager.HEIGHT;
    // }
    this.setState({
      statusBarHeight: Platform.select({
        ios: IOS_STATUS_BAR_HEIGHT,
        android: 0,
      }),
    });
  }

  render() {
    return (
      <Animated.View
        style={[styles.statusBar(this.state.statusBarHeight), this.props.style]}
      />
    );
  }
}
