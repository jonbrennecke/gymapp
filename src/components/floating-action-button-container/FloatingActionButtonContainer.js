// @flow
import React, { Component } from 'react';
import { View } from 'react-native';

import type { Children } from '../../types';

type Props = {
  children?: Children,
};

const styles = {
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingBottom: 20,
    paddingRight: 20,
  },
};

export default class FloatingActionButtonContainer extends Component<
  Props,
  {}
> {
  render() {
    return (
      <View style={styles.container} pointerEvents="box-none">
        {this.props.children}
      </View>
    );
  }
}
