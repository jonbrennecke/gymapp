// @flow
import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { TEXT_COLORS, UI_COLORS, FONTS } from '../../constants';
import { formatActivityMeasurement } from '../../utils/ActivityUtil';

import type { Activity } from '../../types';

type Props = {
  activity: Activity,
};

const styles = {
  container: {
    paddingHorizontal: 21,
    marginVertical: 15,
  },
  title: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  titleText: {
    fontSize: 27,
    fontFamily: FONTS.PT_SANS_BOLD,
    color: TEXT_COLORS.DARK_GREY,
    flex: 1,
  },
  labels: {
    flex: 0.5,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  labelText: {
    fontSize: 17,
    color: TEXT_COLORS.WHITE,
    fontFamily: FONTS.PT_SANS_BOLD,
  },
  label: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
    paddingVertical: 2,
    borderRadius: 15,
  },
  statsContainer: {
    backgroundColor: UI_COLORS.GREEN,
  },
  timestampText: {
    fontSize: 14,
    color: TEXT_COLORS.LIGHT_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
  },
};

export default class ActivityListItem extends Component<Props, {}> {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.title}>
          <Text style={styles.titleText} numberOfLines={1}>
            {`${this.props.activity.displayName} (${formatActivityMeasurement(
              this.props.activity.measurement
            )} x${this.props.activity.repititions})`}
          </Text>
          <View style={styles.labels}>
            <View style={[styles.label, styles.statsContainer]}>
              <Text style={styles.labelText}>
                {formatActivityMeasurement(this.props.activity.measurement)}
              </Text>
            </View>
          </View>
        </View>
        <Text style={styles.timestampText}>Some text goes here</Text>
      </View>
    );
  }
}
