// @flow
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { autobind } from 'core-decorators';

import { UI_COLORS, TEXT_COLORS, FONTS } from '../../constants';

import type { Style } from '../../types';

type Props = {
  style?: Style | Style[],
  onPressStartWorkout: () => any,
};

const styles = {
  container: {
    paddingHorizontal: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'transparent',
    paddingHorizontal: 45,
    paddingVertical: 7,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: UI_COLORS.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
  },
  text: {
    color: TEXT_COLORS.WHITE,
    fontSize: 17,
    fontFamily: FONTS.PASSION_ONE,
  },
};

@autobind
export default class WorkoutStartButton extends Component<Props, {}> {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TouchableOpacity
          style={styles.button}
          onPress={this.props.onPressStartWorkout}
        >
          <Text style={styles.text}>START WORKOUT</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
