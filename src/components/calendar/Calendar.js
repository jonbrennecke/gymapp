// @flow
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { autobind } from 'core-decorators';
import first from 'lodash/first';

import * as DateTimeUtil from '../../utils/DateTimeUtil';
import DateSelector from '../date-selector/DateSelector';
import { TEXT_COLORS, FONTS, UI_COLORS } from '../../constants';

import type { Workout } from '../../types';

type Props = {
  workouts: Workout[],
  monthTimestamp: string,
  onRequestSelectNextMonth: () => any,
  onRequestSelectPreviousMonth: () => any,
  onRequestChangeDay: (timestamp: string) => any,
};

type State = {};

const styles = {
  container: {
    flex: 1,
    padding: 7,
    paddingBottom: 28,
  },
  week: {
    flexDirection: 'row',
    flex: 1,
  },
  day: (isSameMonth: boolean, isToday: boolean) => ({
    flex: 1,
    padding: 8,
    height: 50,
    backgroundColor: isToday
      ? UI_COLORS.EXTRA_LIGHT_GREEN
      : isSameMonth
        ? UI_COLORS.WHITE
        : UI_COLORS.EXTRA_LIGHT_GREY,
    opacity: isSameMonth ? 1 : 0.4,
  }),
  dayOfMonthText: {
    color: TEXT_COLORS.MEDIUM_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
  },
  daysOfWeek: {
    flexDirection: 'row',
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: UI_COLORS.LIGHT_GREY,
    marginBottom: 8,
  },
  dayOfWeek: {
    flex: 1,
    padding: 8,
  },
  dayOfWeekText: {
    color: TEXT_COLORS.DARK_GREY,
    fontFamily: FONTS.PT_SANS_BOLD,
    fontWeight: 'bold',
  },
  workoutIndicator: {
    height: 8,
    width: 8,
    marginTop: 8,
    borderRadius: 4,
    backgroundColor: UI_COLORS.GREEN,
  },
};

@autobind
export default class Calendar extends Component<Props, State> {
  render() {
    return (
      <View style={styles.container}>
        <DateSelector
          date={this.props.monthTimestamp}
          formatDate={DateTimeUtil.formatMonth}
          onRequestSelectPreviousDate={this.props.onRequestSelectPreviousMonth}
          onRequestSelectNextDate={this.props.onRequestSelectNextMonth}
          onPressCurrentDate={() =>
            this.props.onRequestChangeDay(DateTimeUtil.timestamp())
          }
        />
        <View style={styles.daysOfWeek}>
          {mapFirstWeekOfMonth(this.props.monthTimestamp, this.renderDayOfWeek)}
        </View>
        {mapWeeksAroundMonth(this.props.monthTimestamp, startOfWeek => (
          <View key={startOfWeek} style={styles.week}>
            {mapDaysOfWeek(startOfWeek, this.renderDay)}
          </View>
        ))}
      </View>
    );
  }

  renderDayOfWeek(dayTimestamp: string) {
    return (
      <View key={dayTimestamp} style={styles.dayOfWeek}>
        <Text style={styles.dayOfWeekText}>
          {DateTimeUtil.formatDayOfWeek(dayTimestamp)}
        </Text>
      </View>
    );
  }

  renderDay(dayTimestamp: string) {
    const hasWorkoutsOnDay = !!this.props.workouts.find(w =>
      DateTimeUtil.isSameDay(w.startedTimestamp, dayTimestamp)
    );
    return (
      <TouchableOpacity
        key={dayTimestamp}
        style={styles.day(
          DateTimeUtil.isSameMonth(dayTimestamp, this.props.monthTimestamp),
          DateTimeUtil.isSameDay(dayTimestamp, DateTimeUtil.timestamp())
        )}
        onPress={() => this.props.onRequestChangeDay(dayTimestamp)}
      >
        <Text style={styles.dayOfMonthText}>
          {DateTimeUtil.formatDay(dayTimestamp)}
        </Text>
        {hasWorkoutsOnDay && <View style={styles.workoutIndicator} />}
      </TouchableOpacity>
    );
  }
}

function mapFirstWeekOfMonth<T>(monthTimestamp: string, callback: string => T) {
  const weeks = DateTimeUtil.getWeeksAroundMonth(monthTimestamp);
  const firstWeek = first(weeks);
  return mapDaysOfWeek(firstWeek, callback);
}

function mapWeeksAroundMonth<T>(monthTimestamp: string, callback: string => T) {
  const weeks = DateTimeUtil.getWeeksAroundMonth(monthTimestamp);
  return weeks.map(callback);
}

function mapDaysOfWeek<T>(weekTimestamp: string, callback: string => T) {
  const days = DateTimeUtil.getDaysInWeek(weekTimestamp);
  return days.map(callback);
}
