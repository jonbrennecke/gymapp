// @flow
import React, { Component } from 'react';
import { View, Text, PanResponder, Animated, Platform } from 'react-native';
import first from 'lodash/first';
import last from 'lodash/last';
import toInteger from 'lodash/toInteger';
import clamp from 'lodash/clamp';
import { autobind } from 'core-decorators';

import { UI_COLORS, TEXT_COLORS, FONTS } from '../../constants';

import type { Gesture, Style } from '../../types';

type Props = {
  style?: Style | Style[],
  disabled?: boolean,
  initialValue: number,
  range: number[],
  getTickMessage: number => string,
  getHandleTickMessage: number => string,
  onChangeValue: number => any,
};

type State = {
  pan: Animated.ValueXY,
  handleAnim: Animated.Value,
  isDragging: boolean,
  draggableAreaWidth: number,
  value: number,
};

const ESTIMATED_INITIAL_DRAGGABLE_AREA_WIDTH = 100;
const HANDLE_SCALE_MAX = 1.7;

const styles = {
  container: {
    height: 50,
    justifyContent: 'center',
    overflow: 'visible',
  },
  touchableDragArea: {
    position: 'absolute',
    left: 0,
    right: 0,
    height: 50,
    zIndex: 2,
    justifyContent: 'center',
    // overflow: 'visible'
  },
  handle: (
    isDragging: boolean,
    containerWidth: number,
    left: Animated.Value,
    scale: Animated.Value
  ) => ({
    zIndex: 2,
    position: 'absolute',
    top: 0,
    height: 28,
    width: 28,
    marginTop: 11,
    borderRadius: 14,
    borderWidth: 2,
    ...Platform.select({
      ios: {
        borderColor: UI_COLORS.GREEN,
      },
      android: {
        borderColor: 'transparent',
      },
    }),
    backgroundColor: UI_COLORS.WHITE,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: isDragging ? 3 : 0,
    shadowColor: UI_COLORS.DARK_GREY,
    shadowOpacity: 0.15,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 7,
    left: Animated.add(
      Animated.diffClamp(left, 37.5, containerWidth - 30),
      -22.5
    ),
    transform: [{ scale }],
  }),
  bar: {
    backgroundColor: UI_COLORS.GREEN,
    maxHeight: 30,
    borderRadius: 15,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 13,
    paddingVertical: 3,
  },
  tickText: {
    color: TEXT_COLORS.WHITE,
    fontFamily: FONTS.PASSION_ONE,
    fontSize: 17,
  },
  handleTickText: {
    color: TEXT_COLORS.DARK_GREY,
    fontFamily: FONTS.PASSION_ONE,
    fontSize: 17,
  },
  startTickLabel: (pan: Animated.Value, width: number) => ({
    transform: [
      {
        scale: pan.interpolate({
          inputRange: [0, width * 0.25, width],
          outputRange: [0, 1, 1],
        }),
      },
    ],
  }),
  endTickLabel: (pan: Animated.Value, width: number) => ({
    transform: [
      {
        scale: pan.interpolate({
          inputRange: [0, width * 0.75, width],
          outputRange: [1, 1, 0],
        }),
      },
    ],
  }),
};

@autobind
export default class NumberRangeSlide extends Component<Props, State> {
  panResponder: PanResponder;
  static defaultProps = {
    disabled: false,
    initialValue: 0,
  };

  constructor(props: Props) {
    super(props);
    this.state = {
      pan: new Animated.ValueXY({ x: 0, y: 0 }),
      handleAnim: new Animated.Value(1),
      isDragging: false,
      draggableAreaWidth: ESTIMATED_INITIAL_DRAGGABLE_AREA_WIDTH,
      value: this.props.initialValue,
    };
    this.panResponder = PanResponder.create({
      onStartShouldSetPanResponder: this.handleStart,
      onPanResponderTerminationRequest: () => false,
      onPanResponderMove: this.handleMove,
      onPanResponderGrant: this.handleGrant,
      onPanResponderRelease: this.handleRelease,
      onPanResponderTerminate: this.handleRelease,
    });
  }

  handleStart() {
    return !this.props.disabled;
  }

  handleMove(event: Event, gesture: Gesture) {
    if (this.props.disabled) {
      return;
    }
    const config = {
      listener: this.handleMoveAsync,
      // TODO: useNativeDriver: true,
    };
    return Animated.event(
      [
        null,
        {
          moveX: this.state.pan.x,
        },
      ],
      config
    )(event, gesture);
  }

  handleMoveAsync(event: Event, gesture: Gesture) {
    if (this.props.disabled) {
      return;
    }
    const rangeStart = first(this.props.range);
    const rangeEnd = last(this.props.range);
    const moveX = clamp(gesture.moveX, 0, this.state.draggableAreaWidth);
    const percent = moveX / this.state.draggableAreaWidth;
    const value = toInteger(rangeStart + (rangeEnd - rangeStart) * percent);
    this.setState({ value });
    this.props.onChangeValue(value);
  }

  handleGrant() {
    if (this.props.disabled) {
      return;
    }
    this.animateDragStart();
    this.setState({
      isDragging: true,
    });
  }

  handleRelease() {
    this.animateDragEnd();
    this.setState({
      isDragging: false,
    });
  }

  animateDragStart() {
    Animated.spring(this.state.handleAnim, {
      toValue: HANDLE_SCALE_MAX,
      friction: 5,
      tension: 5,
      velocity: -3,
    }).start();
  }

  animateDragEnd() {
    Animated.spring(this.state.handleAnim, {
      toValue: 1,
      friction: 5,
      tension: 5,
      velocity: -3,
    }).start();
  }

  layoutDragArea({
    nativeEvent: {
      layout: { width },
    },
  }: any) {
    const rangeStart = first(this.props.range);
    const rangeEnd = last(this.props.range);
    const percent = (this.state.value - rangeStart) / (rangeEnd - rangeStart);
    const panValueX = percent * width;
    this.setState({
      pan: new Animated.ValueXY({ x: panValueX, y: 0 }),
      draggableAreaWidth: width,
    });
  }

  render() {
    const { left } = this.state.pan.getLayout();
    return (
      <View style={[styles.container, this.props.style]}>
        <View style={styles.touchableDragArea} onLayout={this.layoutDragArea}>
          <Animated.View
            style={this.getHandleStyle()}
            {...this.panResponder.panHandlers}
          >
            <Text style={styles.handleTickText}>
              {this.props.getHandleTickMessage(this.state.value)}
            </Text>
          </Animated.View>
        </View>
        <View style={styles.bar}>
          <Animated.Text
            style={[
              styles.tickText,
              styles.startTickLabel(left, this.state.draggableAreaWidth),
            ]}
          >
            {this.props.getTickMessage(first(this.props.range))}
          </Animated.Text>
          <Animated.Text
            style={[
              styles.tickText,
              styles.endTickLabel(left, this.state.draggableAreaWidth),
            ]}
          >
            {this.props.getTickMessage(last(this.props.range))}
          </Animated.Text>
        </View>
      </View>
    );
  }

  getHandleStyle(): Style {
    const { left } = this.state.pan.getLayout();
    return styles.handle(
      this.state.isDragging,
      this.state.draggableAreaWidth,
      left,
      this.state.handleAnim
    );
  }
}
