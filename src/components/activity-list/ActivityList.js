// @flow
import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { autobind } from 'core-decorators';

import ActivityListItem from '../activity-list-item/ActivityListItem';

import type { Style, Activity } from '../../types';

type Props = {
  style?: Style | Style[],
  activities: Activity[],
};

const styles = {};

@autobind
export default class ActivityList extends Component<Props, {}> {
  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="always"
        style={[styles.scrollView, this.props.style]}
        contentContainerStyle={styles.scrollViewContent}
        overScrollMode="always"
      >
        {this.props.activities.map(activity => (
          <ActivityListItem key={activity.id} activity={activity} />
        ))}
      </ScrollView>
    );
  }
}
