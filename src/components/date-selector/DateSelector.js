// @flow
import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';

import { TEXT_COLORS } from '../../constants';
import * as FontUtil from '../../utils/FontUtil';

import type { Style } from '../../types';

type Props = {
  style?: Style | Style[],
  date: string,
  formatDate: string => string,
  onRequestSelectNextDate: () => any,
  onRequestSelectPreviousDate: () => any,
  onPressCurrentDate: () => any,
};

const styles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingBottom: 18,
    paddingHorizontal: 18,
  },
  dateText: FontUtil.getFontStyle('heading', { size: 'large' }),
  chevronIcon: {
    color: TEXT_COLORS.DARK_GREY,
  },
  chevronTouchableLeft: {
    flex: 0.5,
    alignItems: 'flex-start',
  },
  chevronTouchableRight: {
    flex: 0.5,
    alignItems: 'flex-end',
  },
  touchableCenter: {
    flex: 1,
    alignItems: 'center',
  },
};

export default class DateSelector extends Component<Props, {}> {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TouchableOpacity
          style={styles.chevronTouchableLeft}
          onPress={this.props.onRequestSelectPreviousDate}
        >
          <Entypo name="chevron-thin-left" style={styles.chevronIcon} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touchableCenter}
          onPress={this.props.onPressCurrentDate}
        >
          <Text style={styles.dateText} numberOfLines={1}>
            {this.props.formatDate(this.props.date)}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.chevronTouchableRight}
          onPress={this.props.onRequestSelectNextDate}
        >
          <Entypo name="chevron-thin-right" style={styles.chevronIcon} />
        </TouchableOpacity>
      </View>
    );
  }
}
