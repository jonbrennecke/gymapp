// @flow
import React, { Component } from 'react';
import { Animated, TouchableOpacity, StyleSheet, Keyboard } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { autobind } from 'core-decorators';
import LinearGradient from 'react-native-linear-gradient';

import { UI_COLORS, TEXT_COLORS } from '../../constants';
import { hexToRgbaString } from '../../utils/ColorUtil';

type EmitterSubscription = any;
type IconSize = 'small' | 'large';
type IconType = 'play' | 'stop' | 'quit' | 'log';

type Props = {
  visible: boolean,
  size: IconSize,
  type: IconType,
  onPress: () => any,
};

type State = {
  anim: Animated.Value,
};

const styles = {
  visibility: (visibility: Animated.Value) => ({
    opacity: visibility,
    transform: [{ scale: visibility }],
  }),
  icon: (size: IconSize) => ({
    fontSize: size === 'large' ? 35 : 15,
    color: TEXT_COLORS.WHITE,
    textAlign: 'center',
  }),
  mainButton: (size: IconSize, type: IconType) => ({
    margin: 10,
    height: size === 'large' ? 75 : 40,
    width: size === 'large' ? 75 : 40,
    borderRadius: 37.5,
    backgroundColor: type === 'stop' ? UI_COLORS.MEDIUM_PINK : UI_COLORS.GREEN,
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 2,
    marginTop: 7,
    shadowOpacity: 0.5,
    shadowColor: type === 'stop' ? UI_COLORS.MEDIUM_PINK : UI_COLORS.GREEN,
    shadowRadius: 26,
    shadowOffset: {
      height: 3,
      width: 1,
    },
  }),
  gradient: {
    ...StyleSheet.absoluteFillObject,
    borderRadius: 37.5,
  },
};

@autobind
export default class FloatingActionButton extends Component<Props, State> {
  static defaultProps = {
    visible: true,
  };

  state = {
    anim: new Animated.Value(0),
  };
  keyboardDidShowListener: ?EmitterSubscription = null;
  keyboardDidHideListener: ?EmitterSubscription = null;

  componentDidMount() {
    if (this.props.visible) {
      return this.animateIn();
    }
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener && this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener && this.keyboardDidHideListener.remove();
  }

  componentWillReceiveProps(nextProps: Props) {
    const visibilityChanged = this.props.visible !== nextProps.visible;
    if (visibilityChanged) {
      if (nextProps.visible) {
        return this.animateIn();
      }
      this.animateOut();
    }
  }

  keyboardDidShow() {
    this.animateOut();
  }

  keyboardDidHide() {
    this.animateIn();
  }

  animateIn() {
    Animated.spring(this.state.anim, {
      toValue: 1,
    }).start();
  }

  animateOut() {
    Animated.spring(this.state.anim, {
      toValue: 0,
    }).start();
  }

  render() {
    return (
      <Animated.View style={styles.visibility(this.state.anim)}>
        <TouchableOpacity
          style={styles.mainButton(this.props.size, this.props.type)}
          onPress={this.props.onPress}
        >
          <LinearGradient
            start={{ x: 1.0, y: 0.5 }}
            end={{ x: 0.0, y: 0.5 }}
            colors={[
              hexToRgbaString(UI_COLORS.DARK_GREY, 0.1),
              hexToRgbaString(UI_COLORS.WHITE, 0.1),
            ]}
            style={styles.gradient}
          />
          <Entypo
            name={getEntypoIconName(this.props.type)}
            style={styles.icon(this.props.size)}
          />
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

function getEntypoIconName(type: IconType) {
  switch (type) {
    case 'play':
      return 'controller-play';
    case 'stop':
      return 'controller-stop';
    case 'quit':
      return 'trash';
    case 'log':
      return 'pencil';
    default:
      break;
  }
}
