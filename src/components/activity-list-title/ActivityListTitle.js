// @flow
import React, { Component } from 'react';
import { Text, Animated } from 'react-native';
import { SCREEN_WIDTH, TEXT_COLORS, FONTS } from '../../constants';

type Props = {
  message: string,
  scrollViewContentOffsetY: Animated.Value,
};

const styles = {
  container: (scrollViewContentOffsetY: Animated.Value) => {
    const width = SCREEN_WIDTH;
    return {
      width,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 15,
      paddingBottom: 15,
      transform: [
        {
          translateX: -width / 2,
        },
        {
          scale: scrollViewContentOffsetY.interpolate({
            inputRange: [0, 100],
            outputRange: [1, 0.5],
          }),
        },
        {
          translateX: width / 2,
        },
      ],
    };
  },
  text: {
    fontSize: 50,
    fontFamily: FONTS.PASSION_ONE,
    color: TEXT_COLORS.DARK_GREY,
  },
};

export default class ActivityListTitle extends Component<Props, {}> {
  render() {
    return (
      <Animated.View
        style={styles.container(this.props.scrollViewContentOffsetY)}
      >
        <Text style={styles.text}>{this.props.message}</Text>
      </Animated.View>
    );
  }
}
