// @flow
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { autobind } from 'core-decorators';

import { UI_COLORS, TEXT_COLORS, FONTS } from '../../constants';

import type { Style } from '../../types';

type Props = {
  style?: Style | Style[],
  onPressEndWorkout: () => any,
  onPressLogActivity: () => any,
  onPressQuitWorkout: () => any,
};

const styles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    height: 60,
    width: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: UI_COLORS.GREEN,
    borderWidth: StyleSheet.hairlineWidth,
    marginHorizontal: 15,
  },
  centerButton: {
    height: 100,
    width: 100,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: UI_COLORS.GREEN,
  },
  buttonText: {
    color: UI_COLORS.GREEN,
    fontSize: 17,
    fontFamily: FONTS.PASSION_ONE,
    textAlign: 'center',
  },
  centerButtonText: {
    color: TEXT_COLORS.WHITE,
    fontSize: 21,
    fontFamily: FONTS.PASSION_ONE,
    textAlign: 'center',
  },
};

// $FlowFixMe
@autobind
export default class WorkoutActionButtons extends Component<Props, {}> {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <TouchableOpacity
          style={styles.button}
          onPress={this.props.onPressQuitWorkout}
        >
          <Text style={styles.buttonText}>QUIT</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.centerButton}
          onPress={this.props.onPressLogActivity}
        >
          <Text style={styles.centerButtonText}>LOG</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={this.props.onPressEndWorkout}
        >
          <Text style={styles.buttonText}>END</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
