// @flow
import React, { Component } from 'react';
import { Modal, View, TouchableWithoutFeedback } from 'react-native';
import { BoxShadow } from 'react-native-shadow';

import { hexToRgbaString } from '../../utils/ColorUtil';
import { UI_COLORS, SCREEN_HEIGHT, SCREEN_WIDTH } from '../../constants';

import type { Element, ChildrenArray } from 'react';

type Props = {
  isVisible: boolean,
  children?: ChildrenArray<Element<*>> | Element<*>,
  onRequestClose: () => any,
};

type State = {};

const MODAL_HEIGHT = SCREEN_HEIGHT - 250;

const styles = {
  container: {
    flex: 1,
    height: MODAL_HEIGHT,
    backgroundColor: UI_COLORS.WHITE,
  },
  backgroundTouchable: {
    flex: 1,
    backgroundColor: hexToRgbaString(UI_COLORS.DARK_GREY, 0.5),
  },
};

const BOX_SHADOW_PARAMS = {
  width: SCREEN_WIDTH,
  height: MODAL_HEIGHT,
  color: '#000',
  border: 100,
  opacity: 0.15,
  x: 0,
  y: 0,
};

export default class TopDownModal extends Component<Props, State> {
  render() {
    return (
      <Modal
        visible={this.props.isVisible}
        transparent={true}
        onRequestClose={this.props.onRequestClose}
      >
        <BoxShadow setting={BOX_SHADOW_PARAMS}>
          <View style={styles.container}>{this.props.children}</View>
        </BoxShadow>
        <TouchableWithoutFeedback onPress={this.props.onRequestClose}>
          <View style={styles.backgroundTouchable} />
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}
