// @flow
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import parseInt from 'lodash/parseInt';
import isNaN from 'lodash/isNaN';
import isFinite from 'lodash/isFinite';
import { autobind } from 'core-decorators';

import * as FontUtil from '../../utils/FontUtil';
import * as DateTimeUtil from '../../utils/DateTimeUtil';
import * as ActivityUtil from '../../utils/ActivityUtil';
import { UI_COLORS } from '../../constants';

import type { Activity } from '../../types';

type Props = {
  activities: Activity[],
};

const styles = {
  container: {
    flexDirection: 'row',
    flex: 1,
  },
  item: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headingText: FontUtil.getFontStyle('heading', {
    contentStyle: 'light-content',
    size: 'small',
  }),
  hr: {
    backgroundColor: UI_COLORS.WHITE,
    height: StyleSheet.hairlineWidth,
    width: 65,
    marginVertical: 10,
  },
  percentText: FontUtil.getFontStyle('heading', {
    contentStyle: 'light-content',
    size: 'large',
  }),
};

@autobind
export default class ActivityStatsHeader extends Component<Props, {}> {
  getWeeklyPercentChangeInWeight(): number {
    const activities = this.props.activities.filter(
      a => a.measurement.tag === 'weight'
    );
    return getWeeklyPercentChangeInAmount(activities);
  }

  getWeeklyPercentChangeInDistance(): number {
    const activities = this.props.activities.filter(
      a => a.measurement.tag === 'distance'
    );
    return getWeeklyPercentChangeInAmount(activities);
  }

  getWeeklyPercentChangeInFrequency(): number {
    return getWeeklyPercentChangeInFrequency(this.props.activities);
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityStatsItem
          label="WEIGHT"
          percentDecimal={this.getWeeklyPercentChangeInWeight()}
        />
        <ActivityStatsItem
          label="ACTIVITIES"
          percentDecimal={this.getWeeklyPercentChangeInFrequency()}
        />
        <ActivityStatsItem
          label="DISTANCE"
          percentDecimal={this.getWeeklyPercentChangeInDistance()}
        />
      </View>
    );
  }
}

type ItemProps = {
  label: string,
  percentDecimal: number,
};

function ActivityStatsItem(props: ItemProps) {
  return (
    <View style={styles.item}>
      <Text style={styles.headingText}>{props.label}</Text>
      <View style={styles.hr} />
      <Text style={styles.percentText}>
        {formatPercent(props.percentDecimal)}
      </Text>
    </View>
  );
}

function formatPercent(percentDecimal: number): string {
  const integer = parseInt(percentDecimal * 100);
  return `${integer}%`;
}

function getWeeklyPercentChangeInAmount(activities: Activity[]): number {
  const activitiesInCurrentWeek = activities.filter(a =>
    DateTimeUtil.isWithinCurrentWeek(a.eventTimestamp)
  );
  const activitiesInPreviousWeek = activities.filter(a =>
    DateTimeUtil.isWithinPreviousWeek(a.eventTimestamp)
  );
  const currentWeekTotal = ActivityUtil.sumByAmount(activitiesInCurrentWeek);
  const previousWeekTotal = ActivityUtil.sumByAmount(activitiesInPreviousWeek);
  const percentChange = currentWeekTotal / previousWeekTotal;
  const sign = previousWeekTotal > currentWeekTotal ? -1 : 1;
  return isNaN(percentChange) || !isFinite(percentChange)
    ? 0
    : sign * percentChange - 1;
}

function getWeeklyPercentChangeInFrequency(activities: Activity[]): number {
  const activitiesInCurrentWeek = activities.filter(a =>
    DateTimeUtil.isWithinCurrentWeek(a.eventTimestamp)
  );
  const activitiesInPreviousWeek = activities.filter(a =>
    DateTimeUtil.isWithinPreviousWeek(a.eventTimestamp)
  );
  const currentWeekTotal = activitiesInCurrentWeek.length;
  const previousWeekTotal = activitiesInPreviousWeek.length;
  const percentChange = currentWeekTotal / previousWeekTotal;
  const sign = previousWeekTotal > currentWeekTotal ? -1 : 1;
  return isNaN(percentChange) || !isFinite(percentChange)
    ? 0
    : sign * percentChange - 1;
}
