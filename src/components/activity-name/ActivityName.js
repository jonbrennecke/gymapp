// @flow
import React from 'react';
import { View, TouchableOpacity, Text } from 'react-native';

import * as DateTimeUtil from '../../utils/DateTimeUtil';
import * as FontUtil from '../../utils/FontUtil';

type Props = {
  name: string,
  timestamp: string,
  onPress: () => any,
};

const styles = {
  activityNameContainer: {
    marginBottom: 17,
  },
  activityGroupNameText: FontUtil.getFontStyle('title'),
  activityDateText: FontUtil.getFontStyle('heading', { size: 'small' }),
};

export default function AvtivityName(props: Props) {
  return (
    <View style={styles.activityNameContainer}>
      <TouchableOpacity onPress={props.onPress}>
        <Text style={styles.activityGroupNameText}>
          {formatActivityDisplayName(props.name)}
        </Text>
      </TouchableOpacity>
      <Text style={styles.activityDateText}>
        {DateTimeUtil.formatTime(props.timestamp)}
      </Text>
    </View>
  );
}

function formatActivityDisplayName(displayName: string): string {
  return displayName;
}
