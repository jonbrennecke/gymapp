// @flow
import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

import { UI_COLORS } from '../../../constants';
import * as DateTimeUtil from '../../../utils/DateTimeUtil';
import * as FontUtil from '../../../utils/FontUtil';
import Button from '../../buttons/button/Button';

import type { Style } from '../../../types';

type Props = {
  style?: Style | Style[],
  date: string,
  lastWorkoutTimestamp: ?string,
  onRequestLogActivity: () => any,
};

const styles = {
  container: {
    marginVertical: 8,
    borderRadius: 6,
    shadowColor: UI_COLORS.DARK_GREY,
    shadowOpacity: 0.03,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 4,
    },
    backgroundColor: UI_COLORS.WHITE,
    paddingHorizontal: 17,
    paddingVertical: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  callToActionText: FontUtil.getFontStyle('call-to-action'),
  button: {
    marginTop: 23,
  },
};

export default class CallToActionCard extends Component<Props, {}> {
  render() {
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={styles.callToActionText}>
          {this.getCallToActionMessage()}
        </Text>
        <Button
          style={styles.button}
          shadowStyle="thin"
          message={this.getCallToActionButtonMessage()}
          onPress={this.props.onRequestLogActivity}
        />
      </View>
    );
  }

  getCallToActionButtonMessage(): string {
    if (!this.props.lastWorkoutTimestamp) {
      return 'Start Here';
    }
    return 'Log a Workout';
  }

  getCallToActionMessage(): string {
    if (!this.props.lastWorkoutTimestamp) {
      return 'Log your first workout today!';
    }
    if (DateTimeUtil.isToday(this.props.date)) {
      return `Your last workout was on ${DateTimeUtil.formatCalendarDate(
        // $FlowFixMe
        this.props.lastWorkoutTimestamp
      )}`;
    }
    return 'Looks like you missed a day!';
  }
}
