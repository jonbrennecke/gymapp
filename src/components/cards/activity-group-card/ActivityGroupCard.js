// @flow
import React from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import first from 'lodash/first';

import * as FontUtil from '../../../utils/FontUtil';
import * as ActivityUtil from '../../../utils/ActivityUtil';
import * as Debug from '../../../utils/DebugUtil';
import Button from '../../buttons/button/Button';
import { FONTS, TEXT_COLORS } from '../../../constants';

import type { Element } from 'react';
import type { Activity, Workout } from '../../../types';

type Props = {
  activities: Activity[],
  onRequestUpdateWorkout: (workout: Workout) => any,
  onRequestLogActivity: () => any,
};

const styles = {
  activityDetailsContainer: {
    flexDirection: 'row',
  },
  activityDetails: {
    paddingVertical: 8,
    height: 80,
    paddingRight: 8,
  },
  activityDetailsPadding: {
    marginLeft: 7,
    paddingRight: 17,
    marginRight: 17,
    borderRightWidth: StyleSheet.hairlineWidth,
    borderRightColor: TEXT_COLORS.LIGHT_GREY,
  },
  activityDetailsAmountText: {
    color: TEXT_COLORS.DARK_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
    fontSize: 22,
  },
  activityDetailsRepsText: {
    color: TEXT_COLORS.LIGHT_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
    fontSize: 17,
    fontWeight: 'bold',
  },
  activityDetailsText: {
    color: TEXT_COLORS.DARK_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
    fontSize: 17,
  },
  logIcon: {
    fontSize: 27,
    color: TEXT_COLORS.MEDIUM_GREY,
    borderRadius: 25,
    borderColor: TEXT_COLORS.MEDIUM_GREY,
    borderWidth: 1,
    height: 35,
    width: 35,
    paddingTop: 3.5,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  activityLogButton: {
    flex: 1,
    marginLeft: 7,
    borderLeftWidth: StyleSheet.hairlineWidth,
    borderLeftColor: TEXT_COLORS.LIGHT_GREY,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activityLogButtonText: FontUtil.getFontStyle('button'),
};

export default function ActivityGroupCard(props: Props): ?Element<*> {
  const activitySetsByReps = ActivityUtil.groupActivitiesByReps(
    props.activities
  );
  const activitySetsByAmount = ActivityUtil.groupActivitiesByMeasurementAmount(
    props.activities
  );
  const numberOfRepGroups = Object.keys(activitySetsByReps).length;
  const numberOfAmountGroups = Object.keys(activitySetsByAmount).length;
  if (numberOfRepGroups === 0 || numberOfAmountGroups === 0) {
    Debug.logErrorMessage(
      'WorkoutListByDate.renderActivitiesByGrouping called with an empty array.'
    );
    return null;
  }
  if (numberOfRepGroups === 1 && numberOfAmountGroups === 1) {
    return renderActivityGroupOfSameMetrics(props);
  }
  return renderActivityGroupOfDifferentMetrics(props);
}

function renderActivityGroupOfDifferentMetrics({
  activities,
  ...props
}: Props): ?Element<*> {
  if (activities.length === 0) {
    Debug.logErrorMessage(
      'WorkoutListByDate.renderActivityGroupOfDifferentMetrics called with an empty array.'
    );
    return null;
  }
  return (
    <View style={styles.activityDetailsContainer}>
      <ScrollView horizontal>
        {activities.map((a, i) => (
          <View
            key={a.id}
            style={[
              styles.activityDetails,
              i < activities.length - 1 && styles.activityDetailsPadding,
            ]}
          >
            <Text style={styles.activityDetailsAmountText} numberOfLines={1}>
              {ActivityUtil.formatActivityMeasurement(a.measurement)}
            </Text>
            <Text style={styles.activityDetailsRepsText} numberOfLines={1}>
              {`x${a.repititions}`}
            </Text>
          </View>
        ))}
      </ScrollView>
      {activities.length < 3 && (
        <View style={styles.activityLogButton}>
          <Button message="Log More" onPress={props.onRequestLogActivity} />
        </View>
      )}
    </View>
  );
}

function renderActivityGroupOfSameMetrics({
  activities,
  ...props
}: Props): ?Element<*> {
  if (activities.length === 0) {
    Debug.logErrorMessage(
      'WorkoutListByDate.renderActivityGroupOfSameMetrics called with an empty array.'
    );
    return null;
  }
  const activity: Activity = first(activities);
  const numberOfReps = activity.repititions;
  const numberOfSets = activities.length;
  const amount = ActivityUtil.formatActivityMeasurement(activity.measurement);
  return (
    <View style={styles.activityDetailsContainer}>
      <View style={styles.activityDetails}>
        <Text style={styles.activityDetailsAmountText} numberOfLines={1}>
          {amount}
        </Text>
        <Text style={styles.activityDetailsRepsText} numberOfLines={1}>
          {numberOfSets > 1
            ? `${numberOfSets} x${numberOfReps}`
            : `x${numberOfReps}`}
        </Text>
      </View>
      <View style={styles.activityLogButton}>
        <Button message="Log More" onPress={props.onRequestLogActivity} />
      </View>
    </View>
  );
}
