// @flow
import React, { Component } from 'react';
import { View } from 'react-native';
import map from 'lodash/map';
import first from 'lodash/first';
import { autobind } from 'core-decorators';

import * as ActivityUtil from '../../../utils/ActivityUtil';
import EditableWorkoutTitle from '../../editable-workout-title/EditableWorkoutTitle';
import ActivityGroupCard from '../activity-group-card/ActivityGroupCard';
import ActivityName from '../../activity-name/ActivityName';
import { UI_COLORS } from '../../../constants';

import type { Workout, Activity } from '../../../types';
import type { Element } from 'react';

type Props = {
  activities: Activity[],
  workout: Workout,
  onRequestUpdateWorkout: (workout: Workout) => any,
  onRequestLogActivity: () => any,
  onRequestShowActivityModal: (activityNameSearch: string) => any,
};

const styles = {
  workout: {
    marginBottom: 15,
  },
  card: {
    marginVertical: 8,
    borderRadius: 6,
    shadowColor: UI_COLORS.DARK_GREY,
    shadowOpacity: 0.03,
    shadowRadius: 5,
    shadowOffset: {
      width: 1,
      height: 4,
    },
    backgroundColor: UI_COLORS.WHITE,
    paddingHorizontal: 17,
    paddingTop: 13,
    paddingBottom: 12,
    alignItems: 'flex-start',
  },
};

@autobind
export default class WorkoutCard extends Component<Props, {}> {
  mapActivityGroupsByWorkout(
    workout: Workout,
    render: (Activity[], groupName: string) => ?Element<*>
  ): [] {
    return map(this.getActivityGroupsByWorkout(workout), render);
  }

  getActivityGroupsByWorkout(workout: Workout): { [key: string]: Activity[] } {
    return ActivityUtil.groupActivitiesByDisplayName(
      this.getActivitiesByWorkout(workout)
    );
  }

  getActivitiesByWorkout(workout: Workout): Activity[] {
    return this.props.activities.filter(a => a.workout === workout.id);
  }

  render() {
    return (
      <View style={styles.workout}>
        <EditableWorkoutTitle
          workout={this.props.workout}
          onRequestEditDisplayName={displayName => {
            this.props.onRequestUpdateWorkout({
              ...this.props.workout,
              displayName,
            });
          }}
        />
        {this.mapActivityGroupsByWorkout(
          this.props.workout,
          this.renderActivitiesByGrouping
        )}
      </View>
    );
  }

  renderActivitiesByGrouping(activities: Activity[]): ?Element<*> {
    const activity: Activity = first(activities);
    const groupName = activity.displayName;
    return (
      <View key={groupName} style={styles.card}>
        <ActivityName
          name={groupName}
          timestamp={activity.eventTimestamp}
          onPress={() => this.props.onRequestShowActivityModal(groupName)}
        />
        <ActivityGroupCard
          activities={activities}
          onRequestUpdateWorkout={this.props.onRequestUpdateWorkout}
          onRequestLogActivity={this.props.onRequestLogActivity}
        />
      </View>
    );
  }
}
