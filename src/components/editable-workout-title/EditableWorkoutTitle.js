// @flow
import React, { Component } from 'react';
import { TextInput, Text, TouchableOpacity } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { autobind } from 'core-decorators';

import * as FontUtil from '../../utils/FontUtil';
import * as DateTimeUtil from '../../utils/DateTimeUtil';
import { TEXT_COLORS } from '../../constants';

import type { Workout } from '../../types';

type Props = {
  workout: Workout,
  onRequestEditDisplayName: (displayName: string) => any,
};

type State = {
  displayName: ?string,
  isEditable: boolean,
};

const styles = {
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  workoutText: {
    ...FontUtil.getFontStyle('heading'),
    marginVertical: 9,
    paddingHorizontal: 4,
  },
  input: {
    ...FontUtil.getFontStyle('heading'),
    flexGrow: 1,
    marginVertical: 9,
    paddingHorizontal: 4,
  },
  saveIcon: {
    color: TEXT_COLORS.MEDIUM_GREY,
    marginLeft: 17,
    fontSize: 25,
  },
  editIcon: {
    color: TEXT_COLORS.MEDIUM_GREY,
    marginLeft: 17,
    fontSize: 14,
  },
};

@autobind
export default class EditableWorkoutTitle extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      displayName: props.workout.displayName || '',
      isEditable: false,
    };
  }

  toggleEditing() {
    this.setState({
      isEditable: !this.state.isEditable,
    });
  }

  handleTextInputOnSubmitEditing() {
    this.setState({ isEditable: false });
    if (this.state.displayName) {
      this.props.onRequestEditDisplayName(this.state.displayName);
    }
  }

  handleEditOrSaveButtonOnPress() {
    if (this.state.isEditable) {
      this.handleTextInputOnSubmitEditing();
      return;
    }
    this.toggleEditing();
  }

  render() {
    return (
      <TouchableOpacity style={styles.container} onPress={this.toggleEditing}>
        {this.state.isEditable
          ? this.renderTextInput()
          : this.renderPlaceholder()}
        <TouchableOpacity onPress={this.handleEditOrSaveButtonOnPress}>
          {this.state.isEditable ? (
            <Entypo name="chevron-with-circle-right" style={styles.saveIcon} />
          ) : (
            <Entypo name="pencil" style={styles.editIcon} />
          )}
        </TouchableOpacity>
      </TouchableOpacity>
    );
  }

  renderPlaceholder() {
    return (
      <Text style={styles.workoutText}>
        {this.props.workout.displayName ||
          `You logged a workout ${DateTimeUtil.formatTime(
            this.props.workout.startedTimestamp
          )}`}
      </Text>
    );
  }

  renderTextInput() {
    return (
      <TextInput
        autoFocus
        disabled={!this.state.isEditable}
        value={this.state.displayName}
        onChangeText={displayName => this.setState({ displayName })}
        onSubmitEditing={this.handleTextInputOnSubmitEditing}
        style={styles.input}
        placeholder="Title your workout"
      />
    );
  }
}
