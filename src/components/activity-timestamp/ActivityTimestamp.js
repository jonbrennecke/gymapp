// @flow
import React from 'react';
import { View, Text } from 'react-native';

import * as DateTimeUtil from '../../utils/DateTimeUtil';
import * as FontUtil from '../../utils/FontUtil';

type Props = {
  timestamp: string,
};

const styles = {
  activityNameContainer: {
    marginBottom: 17,
  },
  activityGroupNameText: FontUtil.getFontStyle('title'),
  activityDateText: FontUtil.getFontStyle('heading', { size: 'small' }),
};

export default function AvtivityTimestamp(props: Props) {
  return (
    <View style={styles.activityNameContainer}>
      <Text style={styles.activityGroupNameText}>
        {DateTimeUtil.formatTime(props.timestamp)}
      </Text>
    </View>
  );
}
