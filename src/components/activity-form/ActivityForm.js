// @flow
import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import { autobind } from 'core-decorators';
import toNumber from 'lodash/toNumber';
import range from 'lodash/range';

import { FONTS } from '../../constants';
import * as DateTimeUtil from '../../utils/DateTimeUtil';
import { weight, distance } from '../../utils/ActivityUtil';
import Button from '../buttons/button/Button';
import NumberRangeSlider from '../number-range-slider/NumberRangeSlider';

import type { Style, ActivityMeasurement } from '../../types';

type Props = {
  style?: Style | Style[],
  workoutTimestamp: string,
  onRequestSaveActivity: (activity: {
    displayName: string,
    eventTimestamp: string,
    measurement: ActivityMeasurement,
    repititions: number,
  }) => any,
};

type State = {
  displayName: string,
  distance: string,
  weight: string,
  measurementType: 'weight' | 'distance',
  repititions: number,
};

const DEFAULT_REPS = 15;

const styles = {
  container: {
    flex: 1,
    paddingHorizontal: 15,
  },
  scrollView: {
    flex: 1,
  },
  scrollViewContent: {},
  row: {
    paddingHorizontal: 5,
    marginVertical: 12,
  },
  column: {
    flexDirection: 'column',
  },
  input: {
    fontSize: 17,
    fontFamily: FONTS.PT_SANS_REGULAR,
    marginVertical: 5,
  },
  label: {
    fontSize: 17,
    fontFamily: FONTS.PT_SANS_REGULAR,
    marginTop: 5,
  },
  contextRow: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
  contextButtonSpacer: {
    width: 5,
  },
  saveButtonContainer: {
    marginTop: 10,
    marginBottom: 5,
  },
  numberRange: {},
};

const MEASUREMENT_OPTIONS = {
  WEIGHT: 'weight',
  DISTANCE: 'distance',
};

// $FlowFixMe
@autobind
export default class ActivityForm extends Component<Props, State> {
  state: State = {
    displayName: '',
    distance: '',
    weight: '',
    measurementType: MEASUREMENT_OPTIONS.WEIGHT,
    repititions: DEFAULT_REPS,
  };
  repsInput: TextInput;
  weightOrDistanceInput: TextInput;

  saveActivity() {
    if (!this.canSave()) {
      return;
    }
    const eventTimestamp = DateTimeUtil.isToday(this.props.workoutTimestamp)
      ? DateTimeUtil.timestamp()
      : this.props.workoutTimestamp;
    this.props.onRequestSaveActivity({
      displayName: this.state.displayName,
      eventTimestamp,
      measurement: this.isWeight()
        ? weight(toNumber(this.state.weight), 'pounds')
        : distance(toNumber(this.state.distance), 'miles'),
      repititions: this.state.repititions,
    });
  }

  canSave(): boolean {
    if (!this.state.displayName || !this.state.repititions) {
      return false;
    }
    return !!(this.isWeight() ? this.state.weight : this.state.distance);
  }

  isWeight(): boolean {
    return this.state.measurementType === MEASUREMENT_OPTIONS.WEIGHT;
  }

  isDistance(): boolean {
    return this.state.measurementType === MEASUREMENT_OPTIONS.DISTANCE;
  }

  render() {
    return (
      <KeyboardAvoidingView
        behavior="padding"
        keyboardVerticalOffset={-500}
        style={[styles.container, this.props.style]}
      >
        <ScrollView
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContent}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
          overScrollMode="always"
        >
          <View style={styles.row}>
            <TextInput
              autoFocus
              autoCapitalize="sentences"
              allowFontScaling={false}
              style={styles.input}
              placeholder="Excercise"
              value={this.state.displayName}
              onChangeText={displayName => this.setState({ displayName })}
              blurOnSubmit
              onSubmitEditing={() => {
                this.weightOrDistanceInput.focus();
              }}
            />
          </View>
          <View style={styles.row}>
            <View style={styles.column}>
              <TextInput
                ref={ref => {
                  this.weightOrDistanceInput = ref;
                }}
                allowFontScaling={false}
                style={styles.input}
                placeholder={this.isWeight() ? 'Weight (lbs)' : 'Distance (mi)'}
                value={
                  this.isWeight() ? this.state.weight : this.state.distance
                }
                onChangeText={text => {
                  if (this.isWeight()) {
                    this.setState({ weight: text });
                    return;
                  }
                  this.setState({ distance: text });
                }}
                blurOnSubmit={false}
                onSubmitEditing={() => this.canSave() && this.saveActivity()}
                keyboardType="numeric"
              />
              <View style={styles.contextRow}>
                <Button
                  message="Weight"
                  disabled={this.isWeight()}
                  onPress={() =>
                    this.setState({
                      measurementType: MEASUREMENT_OPTIONS.WEIGHT,
                    })
                  }
                />
                <View style={styles.contextButtonSpacer} />
                <Button
                  message="Distance"
                  disabled={this.isDistance()}
                  onPress={() =>
                    this.setState({
                      measurementType: MEASUREMENT_OPTIONS.DISTANCE,
                    })
                  }
                />
              </View>
            </View>
          </View>
          <View style={styles.row}>
            <Text style={styles.label}>Reps</Text>
            <NumberRangeSlider
              style={styles.numberRange}
              initialValue={this.state.repititions}
              range={range(1, 51)}
              getTickMessage={x => (x > 1 ? `${x} REPS` : '1 REP')}
              getHandleTickMessage={x => `${x}`}
              onChangeValue={reps => this.setState({ repititions: reps })}
            />
          </View>
          <View style={[styles.row, styles.saveButtonContainer]}>
            <Button
              size="large"
              disabled={!this.canSave()}
              message="Save"
              onPress={this.saveActivity}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
