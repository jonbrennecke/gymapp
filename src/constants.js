// @flow
import { Dimensions, Platform } from 'react-native';

export const { height: SCREEN_HEIGHT, width: SCREEN_WIDTH } = Dimensions.get(
  'window'
);

export const BACKGROUND_LAYER_OPACITY = 0.45;

export const TEXT_COLORS = {
  LIGHT_GREY: '#B7BAE1',
  MEDIUM_GREY: '#50548E',
  DARK_GREY: '#3B3E6F',
  WHITE: '#FFF',
  OFF_WHITE: '#F6F6FD',
};

export const UI_COLORS = {
  EXTRA_LIGHT_GREY: '#f2f2f2',
  LIGHT_GREY: '#B7BAE1',
  MEDIUM_GREY: '#50548E',
  DARK_GREY: '#3B3E6F',
  GREEN: '#51ba81',
  DARK_GREEN: '#216640',
  MEDIUM_GREEN: '#35b772',
  LIGHT_GREEN: '#80e5ae',
  EXTRA_LIGHT_GREEN: '#def9eb',
  WHITE: '#fff',
  OFF_WHITE: '#F6F6FD',
  MEDIUM_PINK: '#FF7A99',
};

// NOTE: IOS 8601 is already moment's default format
export const MOMENT_DEFAULT_TIMESTAMP_FORMAT: ?string = null;
export const MOMENT_MONTH_FORMAT = 'MMMM';
export const MOMENT_DAY_FORMAT = 'M/D';
export const MOMENT_DAY_OF_WEEK_FORMAT = 'dd';

export const MOMENT_DAY_MONTH_CALENDAR_FORMAT = {
  sameDay: '[Today], MMM D',
  nextDay: '[Tomorrow], MMM D',
  lastDay: '[Yesterday], MMM D',
  lastWeek: 'dddd, MMM D',
  nextWeek: 'dddd, MMM D',
  sameElse: 'dddd, MMM D',
};

export const MOMENT_HOUR_MIN_CALENDAR_FORMAT = {
  sameDay: '[Today] [at] h:mma',
  nextDay: '[Tomorrow] [at] h:mma',
  lastDay: '[Yesterday] [at] h:mma',
  lastWeek: 'dddd [at] h:mma',
  nextWeek: 'dddd [at] h:mma',
  sameElse: 'dddd [at] h:mma',
};

export const FONTS = {
  PASSION_ONE: 'PassionOne-Regular',
  PT_SANS_REGULAR: Platform.select({
    ios: 'PT Sans',
    android: 'PT_Sans-Web-Regular',
  }),
  PT_SANS_BOLD: Platform.select({
    ios: 'PT Sans',
    android: 'PT_Sans-Web-Bold',
  }),
};

export const FONT_STYLES = {
  BUTTON_DEFAULT_STYLES: {
    color: TEXT_COLORS.WHITE,
    fontFamily: FONTS.PT_SANS_REGULAR,
    fontSize: 17,
  },
  HEADING_DEFAULT_STYLES: {
    color: TEXT_COLORS.MEDIUM_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
    fontSize: 17,
  },
  HEADING_LIGHT_CONTENT_STYLES: {
    color: TEXT_COLORS.WHITE,
  },
  HEADING_SMALL_FONT_SIZE_STYLES: {
    fontSize: 13,
  },
  HEADING_LARGE_FONT_SIZE_STYLES: {
    fontSize: 23,
  },
  CALL_TO_ACTION_FONT_STYLES: {
    color: TEXT_COLORS.MEDIUM_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
    fontSize: 23,
    textAlign: 'center',
  },
  TITLE_FONT_STYLES: {
    color: TEXT_COLORS.MEDIUM_GREY,
    fontFamily: FONTS.PT_SANS_REGULAR,
    fontSize: 23,
    fontWeight: 'bold',
    letterSpacing: 1.2,
  },
};

export const SCREENS = {
  WORKOUT_SCREEN: {
    screen: 'gymlog.WorkoutScreen',
    title: 'WORKOUT',
    animated: true,
    backButtonTitle: '',
  },
  LOG_ACTIVITY_SCREEN: {
    screen: 'gymlog.LogActivityScreen',
    title: 'Log Activity',
    animated: true,
    animationType: 'slide-horizontal',
    backButtonTitle: '',
  },
  ACTIVITY_MODAL: {
    screen: 'gymlog.ActivityModal',
    navigatorStyle: {
      navBarHidden: true,
      animated: true,
      animationType: 'slide-up',
      screenBackgroundColor: 'transparent',
      modalPresentationStyle: 'overCurrentContext',
    },
  },
  CALENDAR_MODAL: {
    screen: 'gymlog.CalendarModal',
    navigatorStyle: {
      navBarHidden: true,
      animated: false,
      animationType: 'none', // TODO: this does NOT work as of react-native-navigation@1.1.441
      screenBackgroundColor: 'transparent',
      modalPresentationStyle: 'overCurrentContext',
    },
  }
};

export const SCREEN_STYLES = {
  NAVBAR: {
    navBarBackgroundColor: UI_COLORS.WHITE,
    navBarTextColor: TEXT_COLORS.DARK_GREY,
    navBarTextFontSize: 22,
    navBarTextFontFamily: FONTS.PT_SANS_REGULAR,
    navBarTitleTextCentered: true,
    navBarHeight: 70,
    navBarButtonColor: UI_COLORS.DARK_GREY,
    statusBarTextColorScheme: 'light',
  },
};

export const SINGLE_SCREEN_NAVIGATOR_PARAMS = {
  screen: {
    screen: 'gymlog.HomeScreen',
    title: 'Home',
    navigatorStyle: {
      navBarHidden: true,
      ...SCREEN_STYLES.NAVBAR,
    },
    navigatorButtons: {},
  },
  appStyle: {
    orientation: 'portrait',
    keepStyleAcrossPush: false,
  },
};

export const INITIAL_STATUS_BAR_HEIGHT = 20;
export const IOS_STATUS_BAR_HEIGHT = 20;
