// @flow
import * as WorkoutUtil from './WorkoutUtil';
import * as ActivityUtil from './ActivityUtil';

import type { Workout, Activity, CardParams } from '../types';

export function getCards(
  workouts: Workout[],
  activities: Activity[],
  timestamp: string
): CardParams[] {
  const workoutsOnDate = WorkoutUtil.filterWorkoutsByDate(workouts, timestamp);
  if (workoutsOnDate.length > 0) {
    const sortedWorkouts = WorkoutUtil.sortByMostRecent(workoutsOnDate);
    const activitiesByWorkout = ActivityUtil.groupActivitiesByWorkout(
      activities
    );
    return sortedWorkouts.map(w => ({
      tag: 'workout',
      workout: w,
      activities: activitiesByWorkout[w.id] || [],
    }));
  } else {
    const mostRecentWorkout = WorkoutUtil.getMostRecentWorkout(workouts);
    return [
      {
        tag: 'call-to-action',
        lastWorkoutTimestamp: mostRecentWorkout
          ? mostRecentWorkout.startedTimestamp
          : null,
      },
    ];
  }
}
