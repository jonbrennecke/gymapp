// @flow
import moment from 'moment';
import {
  MOMENT_DEFAULT_TIMESTAMP_FORMAT,
  MOMENT_MONTH_FORMAT,
  MOMENT_DAY_FORMAT,
  MOMENT_DAY_OF_WEEK_FORMAT,
  MOMENT_DAY_MONTH_CALENDAR_FORMAT,
  MOMENT_HOUR_MIN_CALENDAR_FORMAT,
} from '../constants';

export function timestamp(): string {
  return moment().format(MOMENT_DEFAULT_TIMESTAMP_FORMAT);
}

export function parseTimestamp(
  timestamp: string,
  format?: ?string = MOMENT_DEFAULT_TIMESTAMP_FORMAT
): moment {
  return moment(timestamp, format);
}

export function addDays(timestamp: string, days?: number = 1): string {
  const m = parseTimestamp(timestamp);
  m.add(days, 'days');
  return m.format(MOMENT_DEFAULT_TIMESTAMP_FORMAT);
}

export function subtractDays(timestamp: string, days?: number = 1): string {
  return addDays(timestamp, -days);
}

export function addMonths(timestamp: string, months?: number = 1): string {
  const m = parseTimestamp(timestamp);
  m.add(months, 'months');
  return m.format(MOMENT_DEFAULT_TIMESTAMP_FORMAT);
}

export function subtractMonths(timestamp: string, months?: number = 1): string {
  return addMonths(timestamp, -months);
}

export function isToday(timestampA: string): boolean {
  return isSameDay(timestampA, timestamp());
}

export function isSameDay(timestampA: string, timestampB: string): boolean {
  const a = parseTimestamp(timestampA);
  const b = parseTimestamp(timestampB);
  return a.isSame(b, 'day');
}

export function isSameMonth(timestampA: string, timestampB: string): boolean {
  const a = parseTimestamp(timestampA);
  const b = parseTimestamp(timestampB);
  return a.isSame(b, 'month');
}

export function isWithinCurrentWeek(timestampA: string): boolean {
  const a = parseTimestamp(timestampA);
  const b = parseTimestamp(timestamp());
  return a.isSameOrAfter(b.subtract(7, 'days'));
}

export function isWithinPreviousWeek(timestampA: string): boolean {
  const a = parseTimestamp(timestampA);
  const b = parseTimestamp(timestamp());
  return a.isBetween(
    b.clone().subtract(14, 'days'),
    b.clone().subtract(7, 'days')
  );
}

export function formatCalendarDate(timestamp: string): string {
  const m = parseTimestamp(timestamp);
  return m.calendar(null, MOMENT_DAY_MONTH_CALENDAR_FORMAT);
}

export function formatMonth(timestamp: string): string {
  return parseTimestamp(timestamp).format(MOMENT_MONTH_FORMAT);
}

export function formatDay(timestamp: string): string {
  return parseTimestamp(timestamp).format(MOMENT_DAY_FORMAT);
}

export function formatDayOfWeek(timestamp: string): string {
  return parseTimestamp(timestamp).format(MOMENT_DAY_OF_WEEK_FORMAT);
}

export function formatTime(timestamp: string): string {
  const m = parseTimestamp(timestamp);
  return m.calendar(null, MOMENT_HOUR_MIN_CALENDAR_FORMAT);
}

export function getWeeksAroundMonth(monthTimestamp: string): string[] {
  const m = parseTimestamp(monthTimestamp);
  const startOfMonth = m.startOf('month');
  const weeks = [];
  const acc = startOfMonth.clone();
  while (acc.isSame(startOfMonth, 'month')) {
    weeks.push(acc.format(MOMENT_DEFAULT_TIMESTAMP_FORMAT));
    acc.add(7, 'day');
  }
  return weeks;
}

export function getDaysInWeek(weekTimestamp: string): string[] {
  const m = parseTimestamp(weekTimestamp);
  const startOfWeek = m.startOf('week');
  const days = [];
  const acc = startOfWeek.clone();
  while (acc.isSame(startOfWeek, 'week')) {
    days.push(acc.format(MOMENT_DEFAULT_TIMESTAMP_FORMAT));
    acc.add(1, 'day');
  }
  return days;
}
