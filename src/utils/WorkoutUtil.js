// @flow
import sortBy from 'lodash/sortBy';
import first from 'lodash/first';

import * as DateTimeUtil from './DateTimeUtil';

import type { Workout } from '../types';

export function filterWorkoutsByDate(
  workouts: Workout[],
  timestamp: string
): Workout[] {
  return workouts.filter(w =>
    DateTimeUtil.isSameDay(w.startedTimestamp, timestamp)
  );
}

export function getMostRecentWorkout(workouts: Workout[]): ?Workout {
  const sorted = sortByMostRecent(workouts);
  return first(sorted);
}

export function sortByMostRecent(workouts: Workout[]): Workout[] {
  return sortBy(workouts, (w: Workout) => w.startedTimestamp);
}
