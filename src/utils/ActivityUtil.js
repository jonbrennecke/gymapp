// @flow
import groupBy from 'lodash/groupBy';
import sumBy from 'lodash/sumBy';

import type {
  ActivityMeasurement,
  WeightMeasurement,
  DistanceMeasurement,
  ActivityInterval,
  WeightUnit,
  DistanceUnit,
  Activity,
} from '../types';

export function formatInterval({
  sets,
  repititions,
}: ActivityInterval): string {
  return `${repititions} x ${sets}`;
}

export function formatActivityMeasurement(
  measurement: ActivityMeasurement
): string {
  switch (measurement.tag) {
    case 'weight':
      return formatWeightMeasurement(measurement);
    case 'distance':
      return formatDistanceMeasurement(measurement);
    default:
      return '';
  }
}

export function formatWeightMeasurement({
  amount,
  unit,
}: WeightMeasurement): string {
  return `${amount}${abbreviateWeightUnit(unit, amount > 0)}`;
}

export function formatDistanceMeasurement({
  amount,
  unit,
}: DistanceMeasurement): string {
  return `${amount}${abbreviateDistanceUnit(unit, amount > 0)}`;
}

function abbreviateWeightUnit(unit: WeightUnit, plural: boolean): string {
  switch (unit) {
    case 'pounds':
      return plural ? 'lbs' : 'lb';
    case 'kilograms':
      return plural ? 'kgs' : 'kgs';
    default:
      return '';
  }
}

function abbreviateDistanceUnit(unit: DistanceUnit, plural: boolean): string {
  switch (unit) {
    case 'miles':
      return 'mi';
    case 'feet':
      return 'ft';
    case 'meters':
      return 'm';
    case 'yards':
      return plural ? 'yds' : 'yd';
    default:
      return '';
  }
}

export function weight(amount: number, unit: WeightUnit): WeightMeasurement {
  return {
    tag: 'weight',
    amount,
    unit,
  };
}

export function distance(
  amount: number,
  unit: DistanceUnit
): DistanceMeasurement {
  return {
    tag: 'distance',
    amount,
    unit,
  };
}

export function isTrivialInterval({
  sets,
  repititions,
}: ActivityInterval): boolean {
  return sets === 1 && repititions === 1;
}

export function groupActivitiesByWorkout(
  activities: Activity[]
): { [key: string]: Activity[] } {
  return groupBy(activities, (a: Activity) => a.workout);
}

export function groupActivitiesByDisplayName(
  activities: Activity[]
): { [key: string]: Activity[] } {
  return groupBy(activities, (a: Activity) => a.displayName.toLowerCase());
}

export function groupActivitiesByReps(
  activities: Activity[]
): { [key: string]: Activity[] } {
  return groupBy(activities, 'repititions');
}

export function groupActivitiesByMeasurementAmount(
  activities: Activity[]
): { [key: string]: Activity[] } {
  return groupBy(activities, a => a.measurement.amount);
}

export function sumByAmount(activities: Activity[]): number {
  return sumBy(
    activities,
    (a: Activity) => a.measurement.amount * a.repititions
  );
}
