// @flow

// NOTE: async because the logError API will eventually write to Fabric and/or Sentry
export const logError = async (error: Error) => {
  // eslint-disable-next-line
  console.log(error);
};

export const logErrorMessage = async (errorMessage: string) => {
  // eslint-disable-next-line
  console.log(errorMessage);
};
