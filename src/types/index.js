// @flow
import type { ChildrenArray, Element } from 'react';

export type CardTag = 'call-to-action' | 'workout';
export type CardParams = CallToActionCardParams | WorkoutCardParams;
export type CallToActionCardParams = {
  tag: 'call-to-action',
  lastWorkoutTimestamp: ?string,
};
export type WorkoutCardParams = {
  tag: 'workout',
  workout: Workout,
  activities: Activity[],
};

export type Gesture = {
  moveX: number,
  moveY: number,
  x0: number,
  y0: number,
  dx: number,
  dy: number,
  vx: number,
  vy: number,
};

export type Style = { [key: string]: any };
export type Children = ChildrenArray<Element<*>> | Element<*>;

// redux state types
export type WorkoutState = {
  workouts: { [key: string]: Workout },
};

export type ActivityState = {
  activities: Activity[],
};

export type AppState = {
  workoutState: WorkoutState,
  activityState: ActivityState,
};

// redux action types
export type ReceiveWorkoutPayload = { workout: Workout };
export type ReceiveWorkoutsPayload = { workouts: Workout[] };
export type ReceiveActivityPayload = { activity: Activity };
export type ReceiveActivitiesPayload = { activities: Activity[] };

// redux types
// TODO: replace PayloadType with union payload types above
export type GetState = () => AppState;
export type Action<PayloadType> = { +type: string, payload?: PayloadType };
export type DispatchAction<PayloadType> = (Action<PayloadType>) => PayloadType; // TODO: does it return PayloadType or Action<PayloadType>?
export type PromiseAction<PayloadType> = Promise<Action<PayloadType>>;
export type ThunkAction<PayloadType> = (Dispatch<PayloadType>, GetState) => any;
export type Dispatch<PayloadType> = (
  Action<PayloadType> | ThunkAction<PayloadType> | PromiseAction<PayloadType>
) => any;

export type Id = string;
export type Uuid = string;

export type RealmWorkout = {
  id: Id,
  uuid: Uuid,
  createdBy: string,
  createdTimestamp: string,
  updatedTimestamp: string,
  startedTimestamp: string,
  endedTimestamp: string,
  displayName: ?string,
};

export type Workout = {
  id: Id,
  uuid: Uuid,
  createdBy: string,
  createdTimestamp: string,
  updatedTimestamp: string,
  startedTimestamp: string,
  endedTimestamp: ?string,
  displayName: ?string,
};

export type WorkoutCreate = {
  startedTimestamp: string,
};

export type ActivityCreate = {
  displayName: string,
  workout: Id,
  eventTimestamp: string,
  measurement: ActivityMeasurement,
  repititions: number,
};

export type Activity = {
  id: Id,
  uuid: Uuid,
  workout: Id,
  createdBy: string,
  displayName: string,
  createdTimestamp: string,
  updatedTimestamp: string,
  eventTimestamp: string,
  measurement: ActivityMeasurement,
  repititions: number,
};

export type RealmActivity = {
  id: Id,
  uuid: Uuid,
  workout: Id,
  createdBy: string,
  displayName: string,
  createdTimestamp: string,
  updatedTimestamp: string,
  eventTimestamp: string,
  measurementTag: string,
  measurementAmount: number,
  measurementUnit: WeightUnit,
  repititions: number,
};

export type ActivityInterval = {
  repititions: number,
  sets: number,
};

export type ActivityMeasurement = WeightMeasurement | DistanceMeasurement;

export type WeightMeasurement = {
  tag: 'weight',
  amount: number,
  unit: WeightUnit,
};

export type DistanceMeasurement = {
  tag: 'distance',
  amount: number,
  unit: DistanceUnit,
};

export type Weight = {
  amount: number,
  unit: WeightUnit,
};

export type Distance = {
  amount: number,
  unit: DistanceUnit,
};

// Units

export type WeightUnit = 'pounds' | 'kilograms';

export type DistanceUnit = 'miles' | 'feet' | 'meters' | 'yards';
